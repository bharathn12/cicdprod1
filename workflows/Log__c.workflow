<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Is_Admin_Notified_to_True</fullName>
        <field>Is_Admin_Notified__c</field>
        <literalValue>1</literalValue>
        <name>Update Is Admin Notified to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_ExceptionSupportQueue</fullName>
        <field>OwnerId</field>
        <lookupValue>Exception_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to ExceptionSupportQueue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Logs Flow on Create</fullName>
        <actions>
            <name>Update_Is_Admin_Notified_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Owner_to_ExceptionSupportQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>WF Rule to Update Owner to Support Queue and Check Admin Notified.</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
