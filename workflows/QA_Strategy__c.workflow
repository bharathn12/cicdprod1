<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Post_approval_action</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Post approval action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>rejection_reason</fullName>
        <field>Status__c</field>
        <literalValue>Invalid</literalValue>
        <name>rejection reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
