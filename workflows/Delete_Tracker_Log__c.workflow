<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Notify_Outbound_for_the_Deleted_SME_Acco</fullName>
        <apiVersion>39.0</apiVersion>
        <endpointUrl>https://dummy.qbr.api.qantas.com/sme/notification/smeaccountdelete?access_token=51f159ea-ace8-48d9-9864-0c905414748d</endpointUrl>
        <fields>ABN__c</fields>
        <fields>Id</fields>
        <fields>RecordName__c</fields>
        <fields>SObjectId__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>api_integration_user@qantas.com.au</integrationUser>
        <name>Notify Outbound for the Deleted SME Acco</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Trigger Outbound For  Deleted SME Accounts</fullName>
        <actions>
            <name>Notify_Outbound_for_the_Deleted_SME_Acco</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Triggers the Outbound when the SME Account is deleted which is sent to the Notification Queue-STME 1909</description>
        <formula>$User.Alias != &apos;apiuser&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>WF_Retrigger_SME_Outbound</fullName>
        <actions>
            <name>Notify_Outbound_for_the_Deleted_SME_Acco</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow to Retry_SME_Outbound</description>
        <formula>$User.Alias != &apos;apiuser&apos; &amp;&amp; Trigger_Outbound__c == true &amp;&amp;  RecordType.DeveloperName ==&apos;SMEAccount&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
