<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>AOwnership Update Required flag</fullName>
        <active>false</active>
        <formula>OR( ISCHANGED( Business_Development_Rep__c ), ISCHANGED( Account_Manager__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
