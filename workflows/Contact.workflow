<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Init_caps_first_name</fullName>
        <description>To have the first letter, letter after hyphen, and letter after space as caps</description>
        <field>FirstName</field>
        <formula>IF 
( 
CONTAINS( FirstName , &quot; &quot;), 
( 
UPPER(MID( FirstName , 1,1) ) 
&amp; 
LOWER(MID(FirstName,2, FIND(&quot; &quot;, FirstName) -2)) 
&amp; 
UPPER(MID (FirstName, FIND(&quot; &quot;, FirstName),2)) 
&amp; 
LOWER(MID(FirstName, FIND(&quot; &quot;, FirstName) +2, 255 )) 
), 

IF 
( 
CONTAINS( FirstName , &quot;-&quot;), 
( 
UPPER(MID( FirstName , 1,1) ) 
&amp; 
LOWER(MID(FirstName,2, FIND(&quot;-&quot;, FirstName) -2)) 
&amp; 
UPPER(MID (FirstName, FIND(&quot;-&quot;, FirstName),2)) 
&amp; 
LOWER(MID(FirstName, FIND(&quot;-&quot;, FirstName) +2, 255 )) 
), 
IF 
( 
CONTAINS( FirstName , &quot;&apos;&quot;), 
( 
UPPER(MID( FirstName , 1,1) ) 
&amp; 
LOWER(MID(FirstName,2, FIND(&quot;&apos;&quot;, FirstName) -2)) 
&amp; 
UPPER(MID (FirstName, FIND(&quot;&apos;&quot;, FirstName),2)) 
&amp; 
LOWER(MID(FirstName, FIND(&quot;&apos;&quot;, FirstName) +2, 255 )) 
), 
UPPER(LEFT( FirstName,1))+LOWER(MID(FirstName,2,80))	
) 
) 
)</formula>
        <name>Init caps first name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Init_caps_last_name</fullName>
        <description>To have the first letter, letter after hyphen, and letter after space as caps</description>
        <field>LastName</field>
        <formula>IF 
( 
CONTAINS( LastName , &quot; &quot;), 
( 
UPPER(MID( LastName , 1,1) ) 
&amp; 
LOWER(MID(LastName,2, FIND(&quot; &quot;, LastName) -2)) 
&amp; 
UPPER(MID (LastName, FIND(&quot; &quot;, LastName),2)) 
&amp; 
LOWER(MID(LastName, FIND(&quot; &quot;, LastName) +2, 255 )) 
), 

IF 
( 
CONTAINS( LastName , &quot;-&quot;), 
( 
UPPER(MID( LastName , 1,1) ) 
&amp; 
LOWER(MID(LastName,2, FIND(&quot;-&quot;, LastName) -2)) 
&amp; 
UPPER(MID (LastName, FIND(&quot;-&quot;, LastName),2)) 
&amp; 
LOWER(MID(LastName, FIND(&quot;-&quot;, LastName) +2, 255 )) 
), 
IF 
( 
CONTAINS( LastName , &quot;&apos;&quot;), 
( 
UPPER(MID( LastName , 1,1) ) 
&amp; 
LOWER(MID(LastName,2, FIND(&quot;&apos;&quot;, LastName) -2)) 
&amp; 
UPPER(MID (LastName, FIND(&quot;&apos;&quot;, LastName),2)) 
&amp; 
LOWER(MID(LastName, FIND(&quot;&apos;&quot;, LastName) +2, 255 )) 
), 
UPPER(LEFT( LastName,1))+LOWER(MID(LastName,2,80)) 
) 
) 
)</formula>
        <name>Init caps last name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_Agency</fullName>
        <description>This field update will change the value of Business Type to &apos;Agency&apos;.</description>
        <field>Business_Types__c</field>
        <literalValue>Agency</literalValue>
        <name>Update the business type value to Agency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_B_G</fullName>
        <description>This field update will change the value of Business Type to &apos;B&amp;G&apos;.</description>
        <field>Business_Types__c</field>
        <literalValue>B&amp;G</literalValue>
        <name>Update the business type value to B&amp;G</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_Charte</fullName>
        <description>This field update will change the value of Business Type to &apos;Charter&apos;.</description>
        <field>Business_Types__c</field>
        <literalValue>Charter</literalValue>
        <name>Update the business type value to Charte</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Init caps first name</fullName>
        <actions>
            <name>Init_caps_first_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>Administrator</value>
        </criteriaItems>
        <description>To have the first letter, letter after hyphen, and letter after space as caps</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Init caps last name</fullName>
        <actions>
            <name>Init_caps_last_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>Administrator</value>
        </criteriaItems>
        <description>To have the first letter, letter after hyphen, and letter after space as caps</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to Agency</fullName>
        <actions>
            <name>Update_the_business_type_value_to_Agency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;Agency&apos; when the Account record type is &apos;Agency Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to B%26G</fullName>
        <actions>
            <name>Update_the_business_type_value_to_B_G</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Account,Prospect Account</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;B&amp;G&apos; when the Account record type is &apos;Customer Account&apos; or &apos;Prospect Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to Charter</fullName>
        <actions>
            <name>Update_the_business_type_value_to_Charte</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Charter Account</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;Charter&apos; when the Account record type is &apos;Charter Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
