<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Travel_Agency_to_N</fullName>
        <field>Agency__c</field>
        <literalValue>N</literalValue>
        <name>Account Travel Agency to N</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Travel_Agency_to_Y</fullName>
        <field>Agency__c</field>
        <literalValue>Y</literalValue>
        <name>Account Travel Agency to Y</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_AEQCC_Eligibility_from_Account</fullName>
        <description>To enable a single view of history on Amex responses</description>
        <field>AEQCC_Eligibility_Code__c</field>
        <formula>Account__r.AMEX_response_Code__c</formula>
        <name>Copy AEQCC Eligibility from Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_AEQCC_Response_Code_from_Account</fullName>
        <description>To enable a single view of history on Amex responses</description>
        <field>AEQCC_Response_Code__c</field>
        <formula>Account__r.Qantas_Accept_or_Reject_Code__c</formula>
        <name>Copy AEQCC Response Code from Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prod_Reg_Active_c_to_False</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Prod Reg Active__c to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Prod_Reg_Active_c_to_True</fullName>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <name>Prod Reg Active__c to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Industry_to_Travel</fullName>
        <field>Industry</field>
        <literalValue>Travel</literalValue>
        <name>Update Account Industry to Travel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Direct_Discount_Code</fullName>
        <description>Updates the Key Contact Office ID field</description>
        <field>key_Contact_Office_ID__c</field>
        <formula>Key_Contact_Office_ID__c</formula>
        <name>Update Direct Discount Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_InDirect_Discount_Code</fullName>
        <field>Indirect_Discount_Code__c</field>
        <formula>GDS_QBR_Discount_Code__c</formula>
        <name>Update InDirect Discount Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Max_Airline_Points_to_FALSE</fullName>
        <field>Max_Airline_Points_for_Membership_Year__c</field>
        <literalValue>0</literalValue>
        <name>Update Max. Airline Points to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Max_Airline_Points_to_TRUE</fullName>
        <field>Max_Airline_Points_for_Membership_Year__c</field>
        <literalValue>1</literalValue>
        <name>Update Max. Airline Points to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QBD_Last_Modified_Date_on_Active</fullName>
        <field>QBD_Last_Modified_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update QBD Last Modified Date on Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QBD_reg_last_modified_date</fullName>
        <field>QBD_Last_Modified_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update QBD reg last modified date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_PR_record_type_to_QBD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>QBD_Registration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>set PR record type to QBD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Product_Registraion_Record_Type_t</fullName>
        <field>RecordTypeId</field>
        <lookupValue>AMEX_Registration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>set Product Registraion Record Type t</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Registraion_Record_Type_Aquire</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Aquire_Registration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>set Registraion Record Type Aquire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Account Response and Eligibility codes on Amex Product</fullName>
        <actions>
            <name>Copy_AEQCC_Eligibility_from_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_AEQCC_Response_Code_from_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To enable a single view of history on Amex responses</description>
        <formula>RecordType.DeveloperName == &apos;AMEX_Registration&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Active Status on Active%5F%5Fc</fullName>
        <actions>
            <name>Prod_Reg_Active_c_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND( 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>AEQCC Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.Stage_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.Stage_Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Inactive Status on Active%5F%5Fc</fullName>
        <actions>
            <name>Prod_Reg_Active_c_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND( 3)</booleanFilter>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>AEQCC Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.Stage_Status__c</field>
            <operation>contains</operation>
            <value>Inactive,Expired,Suspended,Blacklisted,Deactivated</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Achieved Max Aquire to FALSE</fullName>
        <actions>
            <name>Update_Max_Airline_Points_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 3) AND 2</booleanFilter>
        <criteriaItems>
            <field>Product_Registration__c.Aquire_Point_Balance__c</field>
            <operation>contains</operation>
            <value>N</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.Aquire_Point_Balance__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>CRM 1351</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Achieved Max Aquire to TRUE</fullName>
        <actions>
            <name>Update_Max_Airline_Points_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Registration__c.Aquire_Point_Balance__c</field>
            <operation>contains</operation>
            <value>Y</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <description>CRM 1351</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Direct Discount Code</fullName>
        <actions>
            <name>Update_Direct_Discount_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To Update the Direct Discount Code Key Contact Office Id</description>
        <formula>RecordType.DeveloperName =&apos;Aquire_Registration&apos; &amp;&amp; ISCHANGED( Key_Contact_Office_ID__c ) &amp;&amp; (ISPICKVAL(Stage_Status__c ,&apos;Active&apos;) || (NOT(ISPICKVAL(Stage_Status__c ,&apos;Active&apos;)) &amp;&amp; Account__r.No_of_Aquires__c == 0))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update InDirect Discount Code</fullName>
        <actions>
            <name>Update_InDirect_Discount_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To Update the InDirect Discount Code</description>
        <formula>RecordType.DeveloperName =&apos;Aquire_Registration&apos; &amp;&amp; ISCHANGED( GDS_QBR_Discount_Code__c ) &amp;&amp; (ISPICKVAL(Stage_Status__c ,&apos;Active&apos;) || (NOT(ISPICKVAL(Stage_Status__c ,&apos;Active&apos;)) &amp;&amp; Account__r.No_of_Aquires__c == 0))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update QBD Enrollment Date on Active</fullName>
        <actions>
            <name>Update_QBD_Last_Modified_Date_on_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Registration__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update QBD Reg%2E Last Modified Date</fullName>
        <actions>
            <name>Update_QBD_reg_last_modified_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED(Active__c),ISCHANGED(QBD_Key_Contact_Name__c),  ISCHANGED(QBD_Key_Contact_Email__c),ISCHANGED(QBD_Business_Communication__c ),ISCHANGED(Key_Contact_Office_ID__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update non Agency Account Travel to N</fullName>
        <actions>
            <name>Account_Travel_Agency_to_N</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Product_Registration__c.Aquire_Industry__c</field>
            <operation>notContain</operation>
            <value>travel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update non Agency Account Travel to Y</fullName>
        <actions>
            <name>Account_Travel_Agency_to_Y</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Industry_to_Travel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Product_Registration__c.Aquire_Industry__c</field>
            <operation>contains</operation>
            <value>travel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product_Registration__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Aquire Registration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>change Product Registraion Record Type to AMEX</fullName>
        <actions>
            <name>set_Product_Registraion_Record_Type_t</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Registration__c.Type__c</field>
            <operation>equals</operation>
            <value>AMEX</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>change Product Registraion Record Type to Aquire</fullName>
        <actions>
            <name>set_Registraion_Record_Type_Aquire</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Registration__c.Type__c</field>
            <operation>equals</operation>
            <value>Aquire</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>change Product Registraion Record Type to QBD</fullName>
        <actions>
            <name>set_PR_record_type_to_QBD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Registration__c.Type__c</field>
            <operation>equals</operation>
            <value>QBD</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
