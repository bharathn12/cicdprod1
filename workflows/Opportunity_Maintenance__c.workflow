<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Unique_Information_Blank</fullName>
        <field>Unique_Information__c</field>
        <name>Unique Information Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unique_Record_Prevention</fullName>
        <field>Unique_Information__c</field>
        <formula>TEXT(Category__c)+ &quot; &quot; + TEXT(Account_Record_Type__c)</formula>
        <name>Unique_Record_Prevention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Unique Information</fullName>
        <actions>
            <name>Unique_Record_Prevention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Active__c, OR( ISPICKVAL(Category__c, &quot;Corporate Airfares&quot;),ISPICKVAL(Category__c, &quot;Qantas Business Savings&quot;)),ISPICKVAL(Account_Record_Type__c, &quot;Customer Account&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Unique Information Blank</fullName>
        <actions>
            <name>Unique_Information_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Active__c = False, OR( ISPICKVAL(Category__c, &quot;Corporate Airfares&quot;), ISPICKVAL(Category__c, &quot;Qantas Business Savings&quot;)),ISPICKVAL(Account_Record_Type__c, &quot;Customer Account&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
