<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reminder_mail_to_agency_contract_approver</fullName>
        <description>Reminder mail to agency contract approver</description>
        <protected>false</protected>
        <recipients>
            <recipient>robharrison@qantas.com.au</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Agency_contract_approval_pending_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Active_Flag_Off1</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>Active Flag Off1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Active_Flag_On</fullName>
        <field>Active__c</field>
        <literalValue>1</literalValue>
        <name>Active Flag On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approve_Rejected_Date</fullName>
        <field>Approved_Rejected_Date__c</field>
        <name>Clear Approve/Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Contracted_Flag_Update</fullName>
        <field>Contracted__c</field>
        <literalValue>1</literalValue>
        <name>Contract Contracted Flag Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Contracted_Flag_Update1</fullName>
        <description>Make Contracted flag Off</description>
        <field>Contracted__c</field>
        <literalValue>0</literalValue>
        <name>Contract Contracted Flag Update1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Record_Type_Change_AR_LC_QBS</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Qantas_Business_Savings</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contract Record Type Change AR- L/C(QBS)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Record_Type_Change_AR_L_C_Adh</fullName>
        <description>Update record type once, the status changes to Approval Required – Legal/ Signature Required by Customer</description>
        <field>RecordTypeId</field>
        <lookupValue>Charters_Contract_Legal_Customer_Pending</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contract Record Type Change AR- L/C(Adh)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Record_Type_Change_Signed_Adho</fullName>
        <description>Signed by customer &amp; change record type to Charters Contract - Signed by Customer</description>
        <field>RecordTypeId</field>
        <lookupValue>Charters_Contract_Signed_by_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contract Record Type Change Signed(Adho)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Record_Type_Change_Signed_QBS</fullName>
        <field>RecordTypeId</field>
        <lookupValue>QBS_Signed_by_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contract Record Type Change Signed(QBS)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Record_Type_Change_Singed_Agen</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Agency_Signed_by_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contract Record Type Change Singed(Agen)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contractflagupdate</fullName>
        <field>Contracted__c</field>
        <literalValue>0</literalValue>
        <name>Contractflagupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_original_contract_end_date</fullName>
        <field>Original_Contract_End_Date__c</field>
        <formula>PRIORVALUE(Contract_End_Date__c)</formula>
        <name>Copy original contract end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Number_of_Extensions</fullName>
        <description>Incriment Number of Extensions based on date</description>
        <field>Number_of_Extensions__c</field>
        <formula>IF(Number_of_Extensions__c&gt;0, Number_of_Extensions__c +1, 1)</formula>
        <name>Increment Number of Extensions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SeT_Contract_ReportType_As_Legal_Custom</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Corporate_Airfares</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SeT Contract ReportType As  Legal/Custom</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_Record_type_Signed_by_Cust</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CA_Signed_by_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Contract Record type-Signed by Cust</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Extended_flag</fullName>
        <field>Extended__c</field>
        <literalValue>1</literalValue>
        <name>Set Extended flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_contract_status_to_Approval_Required</fullName>
        <field>Status__c</field>
        <literalValue>Approval Required – Legal</literalValue>
        <name>Set contract status to Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_contract_status_to_Rejected_Leagal</fullName>
        <field>Status__c</field>
        <literalValue>Rejected – Legal</literalValue>
        <name>Set contract status to Rejected - Leagal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agent_Override_End_Date</fullName>
        <description>Update Agent Override Start Date to Contract End Date</description>
        <field>Agent_Override_End_Date__c</field>
        <formula>Contract_End_Date__c</formula>
        <name>Update Agent Override End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agent_Override_Start_Date</fullName>
        <description>Update Agent Override Start Date to Contract Start Date</description>
        <field>Agent_Override_Start_Date__c</field>
        <formula>Contract_Start_Date__c</formula>
        <name>Update Agent Override Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_status_approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval status to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Valid_Until_Date</fullName>
        <field>Valid_Until_Date__c</field>
        <formula>DATE( YEAR( Contract_Start_Date__c) + FLOOR( ( MONTH ( Contract_Start_Date__c ) + 6 - 1 ) / 12 ), MOD( MONTH ( Contract_Start_Date__c ) + 6 - 1 + IF( DAY ( Contract_Start_Date__c) &gt; CASE( MOD( MONTH( Contract_Start_Date__c ) + 6 - 1, 12 ) + 1, 2, 28, 4, 30, 6, 30, 9, 30, 11, 30, 31 ), 1, 0 ), 12 ) + 1, IF( DAY( Contract_Start_Date__c ) &gt; CASE( MOD( MONTH( Contract_Start_Date__c ) + 6 - 1, 12 ) + 1, 2, 28, 4, 30, 6, 30, 9, 30, 11, 30, 31 ), 1, DAY( Contract_Start_Date__c ) ) )</formula>
        <name>Update Valid Until Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_status_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update approval status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_status_to_Recalled</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Update approval status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_approval_status_to_pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update approval status to pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_rejected_internal</fullName>
        <field>Status__c</field>
        <literalValue>Rejected - Internal</literalValue>
        <name>Update status to rejected internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_sig_req_by_customer</fullName>
        <field>Status__c</field>
        <literalValue>Signature Required by Customer</literalValue>
        <name>Update status to sig. req. by customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_approval_required_internal</fullName>
        <field>Status__c</field>
        <literalValue>Approval Required - Internal</literalValue>
        <name>Update to approval required internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_rejected_internal</fullName>
        <field>Status__c</field>
        <literalValue>Rejected - Internal</literalValue>
        <name>Update to rejected internal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>clear_approved_submission_date_date</fullName>
        <field>Approval_Submission_Date__c</field>
        <name>clear approved submission date date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>contract_status_on_legal_status_clear</fullName>
        <field>Status__c</field>
        <literalValue>Signature Required by Customer</literalValue>
        <name>contract status on legal status clear</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>contractactivetofalse</fullName>
        <field>Active__c</field>
        <literalValue>0</literalValue>
        <name>contractactivetofalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_Contract_Status_to_Signature_Require</fullName>
        <field>Status__c</field>
        <literalValue>Signature Required by Customer</literalValue>
        <name>set Contract Status to Signature Require</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Clear Contract legal date fields</fullName>
        <actions>
            <name>Clear_Approve_Rejected_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>clear_approved_submission_date_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>contract_status_on_legal_status_clear</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear date fields if legal approval is none</description>
        <formula>AND(ISCHANGED(Legal_Approval__c ),  ISPICKVAL(Legal_Approval__c , &apos;&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Make Account Contracted</fullName>
        <actions>
            <name>Active_Flag_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contract_Contracted_Flag_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( Contract_Start_Date__c &lt;= TODAY(), Contract_End_Date__c &gt;= TODAY(), ISPICKVAL( Status__c , &apos;Signed by Customer&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Active_Flag_Off1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Contract_Contracted_Flag_Update1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract__c.Contract_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Make Account Contracted Flag Off</fullName>
        <actions>
            <name>Active_Flag_Off1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contract_Contracted_Flag_Update1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(AND( TODAY() &gt;= Contract_Start_Date__c, TODAY() &lt;= Contract_End_Date__c)), ISPICKVAL( Status__c , &apos;Signed by Customer&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Make Account Contracted Flag On</fullName>
        <actions>
            <name>Active_Flag_On</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contract_Contracted_Flag_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( TODAY() &gt;= Contract_Start_Date__c, TODAY() &lt;= Contract_End_Date__c, ISPICKVAL( Status__c , &apos;Signed by Customer&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Make Account Contracted Scheduled</fullName>
        <active>false</active>
        <formula>AND(Contract_End_Date__c &gt;= TODAY(), ISPICKVAL( Status__c , &apos;Signed by Customer&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Active_Flag_Off1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Contract_Contracted_Flag_Update1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract__c.Contract_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Active_Flag_On</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Contract_Contracted_Flag_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract__c.Contract_Start_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Make Account Contracted Scheduled v1</fullName>
        <active>true</active>
        <formula>AND(Contract_End_Date__c &gt;= TODAY(), ISPICKVAL( Status__c , &apos;Signed by Customer&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Active_Flag_On</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Contract_Contracted_Flag_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract__c.Contract_Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Active_Flag_Off1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Contract_Contracted_Flag_Update1</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contract__c.Contract_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Approval Pending %28Agency%29</fullName>
        <actions>
            <name>Contract_Record_Type_Change_Singed_Agen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 4) AND 3</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signed by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected - Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>equals</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected – Legal</value>
        </criteriaItems>
        <description>CRM - 353 once the status is Approval Required - Legal/ Signature required by Customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Customer Or Legal Approval%28Adhoc%29</fullName>
        <actions>
            <name>Contract_Record_Type_Change_AR_L_C_Adh</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Approval Required – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signature Required by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Adhoc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Customer Or Legal Approval%28CA%29</fullName>
        <actions>
            <name>SeT_Contract_ReportType_As_Legal_Custom</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Approval Required – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signature Required by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Corporate Airfares</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <description>CRM - 56 once the status is Approval Required - Legal/ Signature required by Customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Customer Or Legal Approval%28QBS%29</fullName>
        <actions>
            <name>Contract_Record_Type_Change_AR_LC_QBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Approval Required – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signature Required by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Qantas Business Savings</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <description>CRM - 56 once the status is Approval Required - Legal/ Signature required by Customer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Signed By Customer%28Adhoc%29</fullName>
        <actions>
            <name>Contract_Record_Type_Change_Signed_Adho</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND (4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signed by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected - Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Adhoc</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Signed By Customer%28CA%29</fullName>
        <actions>
            <name>Set_Contract_Record_type_Signed_by_Cust</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signed by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected - Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Corporate Airfares</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <description>CRM - 56 once the status is Signed By Customer, Rejected - Customer &amp; Rejected – Legal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Record Type Change For Signed By Customer%28QBS%29</fullName>
        <actions>
            <name>Contract_Record_Type_Change_Signed_QBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Signed by Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected - Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>equals</operation>
            <value>Rejected – Legal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Type__c</field>
            <operation>equals</operation>
            <value>Qantas Business Savings</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency Account</value>
        </criteriaItems>
        <description>CRM - 56 once the status is Signed By Customer, Rejected - Customer &amp; Rejected – Legal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract with different status</fullName>
        <actions>
            <name>Contractflagupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>contractactivetofalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contract__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Signed by Customer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Increment Number of Extensions</fullName>
        <actions>
            <name>Increment_Number_of_Extensions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Increment Number of Extensions based on date</description>
        <formula>AND(ISCHANGED(Contract_End_Date__c), NOT(ISNULL(PRIORVALUE(Contract_End_Date__c))),ISPICKVAL(Status__c, &apos;Signed by Customer&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Valid Until Date</fullName>
        <actions>
            <name>Update_Valid_Until_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Contract__c.Contract_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contract__c.Override_Valid_Until_Date__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Populate valid until date for 6 months from contract start date</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reminder for Agency Contract Approval Required Pending</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contract__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <description>This is to send the reminder to Approver for Agency Contract which is waiting for action to be taken last 7 days.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_mail_to_agency_contract_approver</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set First Time Extension Fields</fullName>
        <actions>
            <name>Copy_original_contract_end_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Extended_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set First Time Extension Fields</description>
        <formula>AND(ISCHANGED(Contract_End_Date__c), NOT(ISNULL(PRIORVALUE(Contract_End_Date__c))), Extended__c = false, ISPICKVAL(Status__c, &apos;Signed by Customer&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set contract status to Approval Required %E2%80%93 Legal on Legal Approval change</fullName>
        <actions>
            <name>Set_contract_status_to_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract__c.Legal_Approval__c</field>
            <operation>equals</operation>
            <value>Required</value>
        </criteriaItems>
        <description>Set contract status to Approval Required – Legal  on Legal Approval change to Pending</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set contract status to Rejected %E2%80%93 Legal on Legal Approval change</fullName>
        <actions>
            <name>Set_contract_status_to_Rejected_Leagal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract__c.Legal_Approval__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Set contract status to Rejected – Legal  on Legal Approval change to Pending</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set contract status to Signature Required by Customer on legal approval change</fullName>
        <actions>
            <name>set_Contract_Status_to_Signature_Require</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract__c.Legal_Approval__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Set contract status to Signature Required by Customer on legal approval change to Not Required or Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Contract Agent Overide Date</fullName>
        <actions>
            <name>Update_Agent_Override_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Agent_Override_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If the &apos;Agent Override Flag&apos; is ticked, then &quot;Agent Override Start Date&quot; and &quot;Agent Override End Date&quot; should be auto populated based on the contract start and end date. This is only for the AU based accounts.</description>
        <formula>AND(Agent_Override__c = TRUE, Account__r.BillingCountry = &apos;AU&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
