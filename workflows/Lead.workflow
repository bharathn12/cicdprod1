<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Data_Quality_Score_AU_Agency</fullName>
        <field>Data_Quality_Score_for_All__c</field>
        <formula>(IF(LEN( ABN_Tax_Reference__c ) = 0, 0,10) + 
IF(LEN( Legal_Name__c ) = 0, 0,10) + IF( LEN( Street ) = 0, 0,5) + 
IF(LEN( FirstName ) = 0, 0,10) + IF(LEN(  Job_Title__c  ) = 0, 0,5) + IF(ISPICKVAL( Job_Role__c , &quot;&quot;),0,5) + 
IF(ISPICKVAL( Function__c , &quot;&quot;),0,5) + 
IF(OR(LEN( MobilePhone)!=0, LEN( Phone )!=0, LEN( Email )!=0), 10,0) + 
IF(LEN( Parent_Account__c ) = 0,0, 10) + 
IF(LEN( Agency_Sales_Region__c ) = 0,0, 10) + 
IF(ISPICKVAL( Account_Tier__c, &quot;&quot;),0,10) + 
IF(OR(LEN( IATA__c)!=0, LEN( Agency_TIDS__c)!=0),10,0))</formula>
        <name>Update Data Quality Score AU Agency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Data_Quality_Score_Customer</fullName>
        <field>Data_Quality_Score_for_All__c</field>
        <formula>IF(LEN( ABN_Tax_Reference__c ) = 0, 0,15) + 
IF(LEN( Legal_Name__c ) = 0, 0,15) + IF( LEN( Street ) = 0, 0,10) + 
IF(LEN( FirstName ) = 0, 0,15) + IF(ISPICKVAL( Job_Role__c , &quot;&quot;),0,10) + 
IF(ISPICKVAL( Function__c , &quot;&quot;),0,10) + IF( LEN(  Job_Title__c  ) = 0, 0,10) + 
IF(OR(LEN( MobilePhone)!=0, LEN( Phone )!=0, LEN( Email )!=0), 15,0)</formula>
        <name>Update Data Quality Score AU Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Data_Quality_Score_Int</fullName>
        <field>Data_Quality_Score_for_All__c</field>
        <formula>IF(LEN( Legal_Name__c ) = 0, 0,15) + IF( LEN( Street ) = 0, 0,15) + 
IF(LEN( FirstName ) = 0, 0,20) + IF(ISPICKVAL( Job_Role__c , &quot;&quot;),0,10) + 
IF(ISPICKVAL( Function__c , &quot;&quot;),0,10) + IF( LEN( Job_Title__c ) = 0, 0,10) + 
IF(OR(LEN( MobilePhone)!=0, LEN( Phone )!=0, LEN( Email )!=0), 20,0)</formula>
        <name>Update Data Quality Score Int</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Data_Quality_Score_US_UK_Agency</fullName>
        <field>Data_Quality_Score_for_All__c</field>
        <formula>IF(LEN( Legal_Name__c ) = 0, 0,15) + IF( LEN( Street ) = 0, 0,10) + 
IF(LEN( FirstName ) = 0, 0,15) + IF(ISPICKVAL( Job_Role__c , &quot;&quot;),0,10) + 
IF(ISPICKVAL( Function__c , &quot;&quot;),0,10) + IF(LEN(  Job_Title__c ) = 0, 0,10) +
IF(OR(LEN( MobilePhone)!=0, LEN( Phone )!=0, LEN( Email )!=0), 15,0) + 
IF(OR(LEN( IATA__c)!=0, LEN( Agency_TIDS__c)!=0),15,0)</formula>
        <name>Update Data Quality Score US UK Agency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_Agency</fullName>
        <description>This field update will change the value of Business Type to &apos;Agency</description>
        <field>Business_Type__c</field>
        <literalValue>Agency</literalValue>
        <name>Update the business type value to Agency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_B_G</fullName>
        <description>This field update will change the value of Business Type to &apos;B&amp;G&apos;.</description>
        <field>Business_Type__c</field>
        <literalValue>B&amp;G</literalValue>
        <name>Update the business type value to B&amp;G</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_business_type_value_to_Charte</fullName>
        <description>This field update will change the value of Business Type to &apos;Charter&apos;.</description>
        <field>Business_Type__c</field>
        <literalValue>Charter</literalValue>
        <name>Update the business type value to Charte</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Data Quality Score AU Agency</fullName>
        <actions>
            <name>Update_Data_Quality_Score_AU_Agency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>AU</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Data Quality Score AU Customer</fullName>
        <actions>
            <name>Update_Data_Quality_Score_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Customer,Charter,Prospect,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>AU</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Data Quality Score Int non US UK Agency</fullName>
        <actions>
            <name>Update_Data_Quality_Score_Int</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>US,GB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>notEqual</operation>
            <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
            <value>AU,US,GB</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Data Quality Score US UK Agency</fullName>
        <actions>
            <name>Update_Data_Quality_Score_US_UK_Agency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Agency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>equals</operation>
            <value>US,GB</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to Agency</fullName>
        <actions>
            <name>Update_the_business_type_value_to_Agency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Agency</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;Agency&apos; when the New Account Type is &apos;Agency Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to B%26G</fullName>
        <actions>
            <name>Update_the_business_type_value_to_B_G</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Customer,Prospect</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;B&amp;G&apos; when the New Account Type is &apos;Customer Account&apos; or &apos;Prospect Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the business type value to Charter</fullName>
        <actions>
            <name>Update_the_business_type_value_to_Charte</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.New_Account_Type__c</field>
            <operation>equals</operation>
            <value>Charter</value>
        </criteriaItems>
        <description>This workflow will update the Business Type value to &apos;Charter&apos; when New Account Type is &apos;Charter Account&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
