/**********************************************************************************************************************************
 
    Created Date: 27/09/2016
 
    Description: Process Contact trigger for:
                 1. Populate the details at the Account Relationship level automatically upon Contact creation/update
  
    Versión:
    V1.0 - 27/09/2016 - Initial version [FO]
Change History:
======================================================================================================================
Name                          Jira           Description                                             Tag
======================================================================================================================
Priyadharshini Vellingiri    2306    Populate the details at the Account Relationship level          T01
                                     automatically upon Contact creation/update       
**********************************************************************************************************************************/   
trigger AccconRshipUpdate on Contact (after insert, after Update){

    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('AccconRshipUpdate');
        if(ts.Active__c) { 
    if ((trigger.isafter && trigger.isInsert) || (trigger.isafter && trigger.isUpdate)) {
            accConRshipUpdateonContacts.AccContactInsUpdate(trigger.new);
          }
       }
   }catch(Exception e){
        System.debug('Error Occured From AccconRshipUpdate  Trigger: '+e.getMessage());
    }
}