trigger trgTradeSiteResponses on Case (before Insert,after Insert) {

    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('trgTradeSiteResponses');
        if(ts.Active__c){   
            if(trigger.isBefore && trigger.isInsert){
                clsTradesiteHandler.processTradesiteAuthorityNumber(trigger.new);
            }
            if(trigger.isAfter && trigger.isInsert){
                clsTradesiteHandler.processNameChangeRequest(trigger.new);
            }
        }    
    }catch(Exception e){
        System.debug('Error Occured : '+e.getMessage());
    }
}