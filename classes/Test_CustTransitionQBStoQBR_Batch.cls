/**********************************************************************************************************************************
 
    Created Date     :  24/04/17 (DD/MM/YYYY)
    Added By         : Karpagam Swaminathan
    Description      : This class contains unit tests for validating the behavior of CustTransitionQBStoQBR_Batch
    JIRA             : CRM-2522
    Version          :V1.0 - 24/04/17 - Initial version
 
**********************************************************************************************************************************/
@isTest
private class Test_CustTransitionQBStoQBR_Batch {

 //Added By Praveen For test class failure fix
 @testsetup
 static void createData() {
  List < QantasConfigData__c > lstConfigData = new List < QantasConfigData__c > ();
  lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type', 'Exception Logs'));
  insert lstConfigData;
 }

 static testMethod void testCustTransitionQBStoQBR_BatchBatchProcessOtherContact() {

  TestUtilityDataClassQantas.enableTriggers();

  List < Account > accList = new List < Account > ();

  for (Integer i = 0; i < 4; i++) {

   Account acc = (new Account(Name = 'Sample+i', Active__c = true,
    RecordTypeId = TestUtilityDataClassQantas.customerRecordTypeId
   ));

   accList.add(acc);
  }
  Trigger_Status__c cs = new Trigger_Status__c();
  cs.Name = 'AccountTeam';
  cs.Active__c = true;
  //cs.Other fiels values
  insert cs;
  insert accList;
  Contact con = new Contact(FirstName = 'Sample', LastName = 'Test', AccountId = accList[0].Id, Email = 'abc453422@xyz.com', Phone = '12345');
  insert con;

  List < Opportunity > oppList = new List < Opportunity > ();

  oppList.add(new Opportunity(Name = 'Opp1', AccountId = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId, Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(), StageName = 'Qualify', Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  //oppList.add(new Opportunity(Name = 'Opp2', AccountId = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 600000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),StageName = 'Closed - Not Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));
  //oppList.add(new Opportunity(Name = 'Opp3', AccountId = accList[2].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 700000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Qualify',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  //oppList.add(new Opportunity(Name = 'Opp4', AccountId = accList[3].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 800000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Closed - Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));

  insert oppList;

  List < Proposal__c > propList = new List < Proposal__c > ();

  propList.add(new Proposal__c(Name = 'Pro1', Account__c = oppList[0].AccountId, Opportunity__c = oppList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
   MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
   NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
   DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
   Approval_Required_Others__c = false));

  insert propList;

  List < Contract__c > contrList = new List < Contract__c > ();



  contrList.add(new Contract__c(Name = 'Con1', Account__c = propList[0].Account__c, Opportunity__c = propList[0].Opportunity__c, Proposal__c = propList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
   MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'
  ));



  insert contrList;
  String DealSupportRequestRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Deal Support Request').getRecordTypeId();
  List < Case > testCase1 = new List < Case > ();
  testCase1.add(new Case(Related_Contract__c = contrList[0].Id, Distribution_Email__c = 'abc@xyz.com', Type = 'Deal Termination', Status = 'Open', Due_Date__c = date.Today(), AccountId = accList[0].Id, RecordTypeId = DealSupportRequestRecordTypeId, Reasons_for_Termination__c = 'test termination', End_Date__c = Date.today()));
  insert testCase1;
  testCase1[0].Status = 'Closed - Deal Implemented';
  //  testCase1[0].Reasons_for_Termination__c='test termination';
  //  testCase1[0].End_Date__c=Date.today();
  update testCase1;
  Campaign cam = new Campaign(Name = 'QBS to QBR Transition', StartDate = Date.Today(), EndDate = Date.Today() + 30);
  insert cam;
  CampaignMember cm = New CampaignMember(CampaignId = cam.Id, ContactId = con.Id, Status = 'Planned');
  insert cm;

  Test.startTest();
  CustTransitionQBStoQBR_Batch batchObj = new CustTransitionQBStoQBR_Batch();
  Database.executeBatch(batchObj);
  Test.stopTest();
 }
 static testMethod void testCustTransitionQBStoQBR_BatchBatchProcess() {

  TestUtilityDataClassQantas.enableTriggers();



  // Date myDate = Date.newInstance(2016, 9, 15);
  // Date newDate = mydate.addDays(2);



  List < Account > accList = new List < Account > ();

  for (Integer i = 0; i < 4; i++) {

   Account acc = (new Account(Name = 'Sample+i', Active__c = true,
    RecordTypeId = TestUtilityDataClassQantas.customerRecordTypeId
   ));

   accList.add(acc);
  }
  Trigger_Status__c cs = new Trigger_Status__c();
  cs.Name = 'AccountTeam';
  cs.Active__c = true;
  //cs.Other fiels values
  insert cs;
  insert accList;
  Contact con = new Contact(FirstName = 'Sample', LastName = 'Test', AccountId = accList[0].Id, Email = 'abc453422@xyz.com', Job_Role__c = 'Travel Co-Ordinator/Booker');
  insert con;
  Contact con1 = new Contact(FirstName = 'Sample1', LastName = 'Test1', AccountId = accList[1].Id, Email = 'abc1453422@xyz.com', Job_Role__c = 'Travel Co-Ordinator/Booker');
  insert con1;
  List < Opportunity > oppList = new List < Opportunity > ();

  oppList.add(new Opportunity(Name = 'Opp1', AccountId = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId, Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(), StageName = 'Qualify', Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  oppList.add(new Opportunity(Name = 'Opp2', AccountId = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId, Amount = 600000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(), StageName = 'Closed - Not Accepted', Closed_Reasons__c = 'Network', Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));
  //oppList.add(new Opportunity(Name = 'Opp3', AccountId = accList[2].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 700000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Qualify',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  //oppList.add(new Opportunity(Name = 'Opp4', AccountId = accList[3].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 800000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Closed - Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));

  insert oppList;

  List < Proposal__c > propList = new List < Proposal__c > ();

  propList.add(new Proposal__c(Name = 'Pro1', Account__c = oppList[0].AccountId, Opportunity__c = oppList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
   MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
   NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
   DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
   Approval_Required_Others__c = false));

  propList.add(new Proposal__c(Name = 'Pro2', Account__c = oppList[1].AccountId, Opportunity__c = oppList[1].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
   MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
   NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
   DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
   Approval_Required_Others__c = false));
  insert propList;

  List < Contract__c > contrList = new List < Contract__c > ();



  contrList.add(new Contract__c(Name = 'Con1', Account__c = propList[0].Account__c, Opportunity__c = propList[0].Opportunity__c, Proposal__c = propList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
   MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'
  ));


  contrList.add(new Contract__c(Name = 'Con2', Account__c = propList[1].Account__c, Opportunity__c = propList[1].Opportunity__c, Proposal__c = propList[1].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
   MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'
  ));
  insert contrList;
  String DealSupportRequestRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Deal Support Request').getRecordTypeId();
  List < Case > testCase1 = new List < Case > ();
  testCase1.add(new Case(Related_Contract__c = contrList[0].Id, Distribution_Email__c = 'abc@xyz.com', Type = 'Deal Termination', Status = 'Open', Due_Date__c = date.Today(), AccountId = accList[0].Id, RecordTypeId = DealSupportRequestRecordTypeId, Reasons_for_Termination__c = 'test termination', End_Date__c = Date.today()));
  testCase1.add(new Case(Related_Contract__c = contrList[1].Id, Distribution_Email__c = 'abcc@xyz.com', Type = 'Deal Termination', Status = 'Open', Due_Date__c = date.Today(), AccountId = accList[1].Id, RecordTypeId = DealSupportRequestRecordTypeId, Reasons_for_Termination__c = 'test termination', End_Date__c = Date.today()));
  insert testCase1;
  Campaign cam = new Campaign(Name = 'QBS to QBR Transition', StartDate = Date.Today(), EndDate = Date.Today() + 30);
  insert cam;
  testCase1[0].Status = 'Closed - Deal Implemented';
  testCase1[1].Status = 'Closed - Deal Implemented';
  //  testCase1[0].Reasons_for_Termination__c='test termination';
  //  testCase1[0].End_Date__c=Date.today();
  update testCase1;

  // CollaborationGroup ca=new CollaborationGroup(Name='QBS to QBR Transition1',CollaborationType='Private');
  //insert ca;


  Test.startTest();
  CustTransitionQBStoQBR_Batch batchObj = new CustTransitionQBStoQBR_Batch();
  Database.executeBatch(batchObj);
  Test.stopTest();
 }
 static testMethod void testCustTransitionQBStoQBR_BatchBatchProcessValContact() {

  TestUtilityDataClassQantas.enableTriggers();



  // Date myDate = Date.newInstance(2016, 9, 15);
  // Date newDate = mydate.addDays(2);



  List < Account > accList = new List < Account > ();

  for (Integer i = 0; i < 4; i++) {

   Account acc = (new Account(Name = 'Sample+i', Active__c = true,
    RecordTypeId = TestUtilityDataClassQantas.customerRecordTypeId
   ));

   accList.add(acc);
  }
  Trigger_Status__c cs = new Trigger_Status__c();
  cs.Name = 'AccountTeam';
  cs.Active__c = true;
  //cs.Other fiels values
  insert cs;
  insert accList;
  Contact con = new Contact(FirstName = 'Sample', LastName = 'Test', AccountId = accList[0].Id, Email = 'abc453422@xyz.com', Job_Role__c = 'Travel Co-Ordinator/Booker');
  insert con;
  Contact con1 = new Contact(FirstName = 'Sample1', LastName = 'Test1', AccountId = accList[1].Id, Email = 'abc1453422@xyz.com', Job_Role__c = 'Travel Co-Ordinator/Booker');
  insert con1;
  List < Opportunity > oppList = new List < Opportunity > ();

  oppList.add(new Opportunity(Name = 'Opp1', AccountId = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId, Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(), StageName = 'Qualify', Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  oppList.add(new Opportunity(Name = 'Opp2', AccountId = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId, Amount = 600000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(), StageName = 'Closed - Not Accepted', Closed_Reasons__c = 'Network', Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));
  //oppList.add(new Opportunity(Name = 'Opp3', AccountId = accList[2].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 700000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Qualify',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
  //oppList.add(new Opportunity(Name = 'Opp4', AccountId = accList[3].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 800000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Closed - Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));

  insert oppList;

  List < Proposal__c > propList = new List < Proposal__c > ();

  propList.add(new Proposal__c(Name = 'Pro1', Account__c = oppList[0].AccountId, Opportunity__c = oppList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
   MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
   NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
   DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
   Approval_Required_Others__c = false));

  propList.add(new Proposal__c(Name = 'Pro2', Account__c = oppList[1].AccountId, Opportunity__c = oppList[1].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
   MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
   NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
   DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
   Approval_Required_Others__c = false));
  insert propList;

  List < Contract__c > contrList = new List < Contract__c > ();



  contrList.add(new Contract__c(Name = 'Con1', Account__c = propList[0].Account__c, Opportunity__c = propList[0].Opportunity__c, Proposal__c = propList[0].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
   MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'
  ));


  contrList.add(new Contract__c(Name = 'Con2', Account__c = propList[1].Account__c, Opportunity__c = propList[1].Opportunity__c, Proposal__c = propList[1].Id,
   Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
   Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
   MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
   Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'
  ));
  insert contrList;
  String DealSupportRequestRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Deal Support Request').getRecordTypeId();
  List < Case > testCase1 = new List < Case > ();
  testCase1.add(new Case(Related_Contract__c = contrList[0].Id, Distribution_Email__c = 'abc@xyz.com', Type = 'Deal Termination', Status = 'Open', Due_Date__c = date.Today(), AccountId = accList[0].Id, RecordTypeId = DealSupportRequestRecordTypeId, Reasons_for_Termination__c = 'test termination', End_Date__c = Date.today()));
  testCase1.add(new Case(Related_Contract__c = contrList[1].Id, Distribution_Email__c = 'abcc@xyz.com', Type = 'Deal Termination', Status = 'Open', Due_Date__c = date.Today(), AccountId = accList[1].Id, RecordTypeId = DealSupportRequestRecordTypeId, Reasons_for_Termination__c = 'test termination', End_Date__c = Date.today()));
  insert testCase1;
  Campaign cam = new Campaign(Name = 'QBS to QBR Transition', StartDate = Date.Today(), EndDate = Date.Today() + 30);
  insert cam;
  testCase1[0].Status = 'Closed - Deal Implemented';
  testCase1[1].Status = 'Closed - Deal Implemented';
  //  testCase1[0].Reasons_for_Termination__c='test termination';
  //  testCase1[0].End_Date__c=Date.today();
  update testCase1;

  


  Test.startTest();
  CustTransitionQBStoQBR_Batch batchObj = new CustTransitionQBStoQBR_Batch();
  Database.executeBatch(batchObj);
  Test.stopTest();
 }
}