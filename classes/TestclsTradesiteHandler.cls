@isTest
private class TestclsTradesiteHandler{
    //Added By Praveen For test class failure fix
    @testsetup
    static void createData(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        insert lstConfigData;
    }
    
    static testMethod void testAuthorityNumber(){

        TestUtilityDataClassQantas.enableTriggers();

        Integration_User_Tradesite__c testSetting = new Integration_User_Tradesite__c();
        testSetting.name = 'tradeSiteUser';
        testSetting.Username__c = 'sf1';
		testSetting.Password__c = 'pas';
		testSetting.Endpoint__c = 'endpo';
		testSetting.NameChangeStartNumber__c = 1;
		testSetting.NameChangeEndNumber__c = 9;
		testSetting.NameChangeLastNumber__c = 0;
		testSetting.ApprovedText__c = 'Appr';
		testSetting.RejectedText__c = 'Reje';
		insert testSetting;
        
		insert new Trigger_Status__c(name = 'AccountTeam',Active__c = true);        
        
        case c = new case();
        c.TradeSite_Request_Id__c = '1231';
        c.Agency_Code__c = '1231';
        c.Migrated__c = false;
        insert c;
        
        Account a = new Account();
        a.name = 'testTradesiteAccount';
        a.Qantas_Industry_Centre_ID__c = '1231';
        a.type = 'Agency Account';
        insert a; 

        case c2 = new case();
        c2.TradeSite_Request_Id__c = '1231';
        c2.Agency_Code__c = '1231';
        c2.Migrated__c = false;
        insert c2;  
        
        update c2;
        list<case> cases = new list<Case>();
        cases.add(c);
        cases.add(c2);
        clsCaseFieldUpdatesHelper.generateAuthorityNumber(cases);
 
      
    }
    

}