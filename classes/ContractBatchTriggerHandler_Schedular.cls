global class ContractBatchTriggerHandler_Schedular implements Schedulable {
list<Contract__c> Con = new list<Contract__c>();
        global void execute(SchedulableContext sc) {
        Set<id> contractId = new Set<id>();
  for(Contract__c c :Con )
{
contractId.add(c.id);
}
        ContractBatchTriggerHandler opb = new ContractBatchTriggerHandler(contractId);
        Database.executeBatch(opb,100);
        }
}