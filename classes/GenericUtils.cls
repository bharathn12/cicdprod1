/*----------------------------------------------------------------------- 
Author       :  Praveen S
Company      :  Capgemini   
Description  :  This is a Utility class used to manipulate generic functions
Test Class   :  ObjectUtilsTest
Created Date :  01/12/2016
History
<Date>      <Authors Name>     <Brief Description of Change>
-----------------------------------------------------------------------*/
public class GenericUtils {

	public virtual class RecordTypeException extends Exception {}
	// To avoid Multiple Object instance
    private static string sObjectName ='';
    private static Map<String, Schema.RecordTypeInfo> recordTypeInfo;

	/*------------------------------------------------------------
    Method Name:     getObjectRecordTypeId
    Description:   Method to extract the ID of the given record type
    Inputs:        Object, Record Type Name
    ------------------------------------------------------------*/
    public static Id getObjectRecordTypeId(String objectName, String recordTypeName){
        system.debug('objectName======='+objectName+'recordTypeName===='+recordTypeName);
        if(recordTypeInfo == Null )//Added to avoid exception
            recordTypeInfo  = new Map<String, Schema.RecordTypeInfo>();
        if(sObjectName == '' || sObjectName != objectName ){
            sObjectName = objectName;
            recordTypeInfo = getObjectRecordTypeId(objectName);
            if(!recordTypeInfo.containsKey(recordTypeName)){
                throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
            }
            //Retrieve the record type id by name
            return recordTypeInfo.get(recordTypeName).getRecordTypeId();
        }
        else{
            if(!recordTypeInfo.containsKey(recordTypeName))
                throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
            //Retrieve the record type id by name
            return recordTypeInfo.get(recordTypeName).getRecordTypeId();
        }
    }

    /*------------------------------------------------------------
    Method Name:     getObjectRecordTypeId
    Description:   Method to extract the record type information
    Inputs:        Object name
    ------------------------------------------------------------*/
    private static map<String, Schema.RecordTypeInfo> getObjectRecordTypeId(String objectName) {
       // Convert to schema.sObjectType
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.RecordTypeInfo> recordTypeInfos = convertType.getDescribe().getRecordTypeInfosByName();
        return recordTypeInfos;
    }
}