/**********************************************************************************************************************************
 
    Description: This is the Controller for the VFPage "proposalProductEntry". Enables to display the multiple combinations & based on the combination the related Fares.
                 Also allows user to select multiple fares at a time. Providing the discounts for those fares in next page. So finally user can Save/ Save & More/ Cancel.    
    Method Description: 
    1) proposalProductEntryController: To get the Proposal record to controller & Initialization of variables.
    2) updateAvailableList: Based on the combination selected will display all the available Fares.
    3) addToShoppingCart: Add the selected Fares to a new list.
    4) onSelectAll: Selects all Fares once clicking on the Select All check box.
    5) onSave: Once clicking on the Save button it will save all the selected Fares & links those to the Proposal returns the page to Proposal
    6) onSaveMore: Once clicking on the Save button it will save all the selected Fares & links those to the Proposal returns to the previous page.
    7) onCancel: Won't save the Fares & redirects the page to Proposal page.
    8) ProductWrapper: Wrapper class to enable the Fares to select multiple at a time.
    9) onSelect: Once clicking on the Select button selects all the flagged on Fares to the next page to Save those.
    10) pgntionProducts: To paginate all the Fares in a specified count instead of displaying all at a time.
    11) getFareCombinations: Displays all the Fare combinations.
    12) getBookingClassIds: To get the Fares Ids based on the selected Combination from Fare Discount Guidelines object.
    13) fareCombinationChange: On change of Fare Combincation changes all the Fares.
    14) next: Goes to next page to display next set of fares in a paginated manner.
    15) previous: Goes to previous page to display previous set of fares in a paginated manner.
    16) getPreviousDisplay: To enable/ disable previuos button.
    17) getNextDisplay: To enable/ disable next button. 
    
    Versión:

**********************************************************************************************************************************/

public with sharing class proposalProductEntryController {

    public Proposal__c theOpp {get;set;}
    public String searchString {get;set;}
    public List<Discount_List__c> shoppingCart {get;set;}
    public Fare_Structure__c[] AvailableProducts {get;set;}
    public Pricebook2 theBook {get;set;}   
    public List<ProductWrapper> productList {get; set;}
    public Set<ProductWrapper> selectedProducts {get; set;}
    public List<ProductWrapper> limitedProducts {get; set;}
    
    public String toSelect {get; set;}
    public String toUnselect {get; set;}
    public String selectedPicklistValue {get; set;}
    public String pickValue;
    public Decimal Total {get;set;}
    
    public Boolean overLimit {get;set;}
    public Boolean multipleCurrencies {get; set;}
    public Boolean displaySelected {get; set;}
    public Boolean displayAvailable {get; set;}
    public Boolean selectAll {get; set;}
    public Boolean displayFareCombination {get; set;}
    public Boolean errorMsg {get; set;}
    public Boolean disableIndividual {get; set;}
        
    public Integer totalRecs = 0;
    public Integer OffsetSize = 0;
    public Integer LimitSize = 50;
    
    private Boolean forcePricebookSelection = false;
        
    private opportunityLineItem[] forDeletion = new opportunityLineItem[]{};

    /*
    Method Description: To get the Proposal record to controller & Initialization of variables.
    */
    public proposalProductEntryController(ApexPages.StandardController controller) {

        // Need to know if org has multiple currencies enabled
        multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
        
        productList = new List<ProductWrapper>();
        selectedProducts = new Set<ProductWrapper>();
        limitedProducts = new List<ProductWrapper>();
        displayFareCombination = true;
        displayAvailable = true;        

        // Get information about the Opportunity being worked on        
        theOpp = [select Id, RecordTypeId, Type__c from Proposal__c where Id = :controller.getRecord().Id limit 1];
        // Commented for CRM 1990 - As per the Business requirment removed Mainline - H in B deal and added Mainline - Red e-Deal + Q for Corporate Airfares
        // if(theOpp.Type__c == 'Corporate Airfares') selectedPicklistValue = 'Mainline - H in B deal';
        if(theOpp.Type__c == 'Corporate Airfares') selectedPicklistValue = 'Mainline - Red e-Deal + Q';
        if(theOpp.Type__c == 'Qantas Business Savings') selectedPicklistValue = 'Mainline - Red e-Deal';
        shoppingCart = new List<Discount_List__c>();        
        updateAvailableList();
        selectAll = true; onSelectAll();
        disableIndividual = true;        
    }          
    /*
    Method Description: Based on the combination selected will display all the available Fares.
    */    
    public void updateAvailableList(){    
        
        if(selectedPicklistValue != null){
        List<Id> bookingClassIds;
        if(selectedPicklistValue != 'All') bookingClassIds = getBookingClassIds(selectedPicklistValue);
        // We dynamically build a query string and exclude items already in the shopping cart
        String qString = 'select Id, Name, Active__c, Route__c, Category__c,Cabin__c, Discount__c, Network__c, Market__c, ProductCode__c,Prod_Code_RededealQ__c, Qantas_Published_Airfare__c, Segment__c from Fare_Structure__c where Active__c=true ';
        //if(bookingClassIds != null) { if(bookingClassIds.size()>0) qString+= 'and Id IN :bookingClassIds'; }
        if(selectedPicklistValue == 'International') qString+= 'AND Segment__c=\'International\'';
        // if(selectedPicklistValue == 'International') qString+= 'AND (Market__c!=\'AU-INT\') AND (Segment__c=\'International\')';
        // CRM 2402
        if(selectedPicklistValue == 'MCA Route Deal') qString+= 'AND Market__c=\'AU-INT\'';
        if(selectedPicklistValue == 'Non-MCA Route Deal') qString+= 'AND Market__c=\'AU-INT\'';
        if(selectedPicklistValue != 'All' && selectedPicklistValue != 'International') { qString+= 'and Id IN :bookingClassIds' ; }
       
        // modify this to search other fields if desired
        if(searchString!=null){
            qString+= ' and (Name like \'%' + searchString + '%\' or ProductCode__c like \'%' + searchString + '%\' or Prod_Code_RededealQ__c like \'%' + searchString + '%\' or Market__c like \'%' + searchString + '%\' or Cabin__c like \'%' + searchString + '%\' or Qantas_Published_Airfare__c like \'%' + searchString + '%\' or Segment__c like \'%' + searchString + '%\' or Category__c like \'%' + searchString + '%\' )';
        }
        
        //<!-- Commented on 8th April for CRM -54 by Prateek 
        qString+= ' order By ProductCode__c';
       
        
        system.debug('qString:' +qString);        
        AvailableProducts = database.query(qString);
        
        productList.clear();
        for(Fare_Structure__c pe : AvailableProducts){
            ProductWrapper pw = new ProductWrapper(pe);
            productList.add(pw);
        }
        totalRecs = productList.size();
        
        pgntionProducts();
        
        }
    }
    /*
    Method Description: Add the selected Fares to a new list.
    */    
    public void addToShoppingCart(){
    
        // This function runs when a user hits "select" button next to a product
        System.debug('Action Support ** ');
        for(Fare_Structure__c d : AvailableProducts){
            if((String)d.Id==toSelect){
                shoppingCart.add(new Discount_List__c(FareStructure__c = d.Id, Proposal__c = theOpp.Id));
                break;
            }
        }
        
        updateAvailableList();  
    }    
    /*
    Method Description: Selects all Fares once clicking on the Select All check box.
    */
    public void onSelectAll(){
        
        for(ProductWrapper pw : limitedProducts){
            if(selectAll == true) pw.selected = true;
            else if(selectAll == false) pw.selected = false;
        }
    }     
    /*
    Method Description: Once clicking on the Save button it will save all the selected Fares & links those to the Proposal returns the page to Proposal
    */
    public PageReference onSave(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
                upsert(shoppingCart);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    /*
    Method Description: Once clicking on the Save button it will save all the selected Fares & links those to the Proposal returns to the previous page.
    */
    public PageReference onSaveMore(){
    
        // If previously selected products are now removed, we need to delete them
        if(forDeletion.size()>0)
            delete(forDeletion);
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(shoppingCart.size()>0)
                upsert(shoppingCart);
        }
        catch(Exception e){
            System.debug('****'+e.getMessage());
            ApexPages.addMessages(e);
            return null;
        }  
           
        // After save return the user to the Opportunity
        return null;
        //return new PageReference('/apex/opportunityProductEntry?id=' + ApexPages.currentPage().getParameters().get('Id'));
    }
    /*
    Method Description: Won't save the Fares & redirects the page to Proposal page.
    */
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Opportunity   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    /*
    Method Description: Wrapper class to enable the Fares to select multiple at a time.
    */    
    public class ProductWrapper {
        public Boolean selected {get; set;}
        public Fare_Structure__c product {get; set;}
        
        public ProductWrapper(Fare_Structure__c pe){
            product = pe;
        }
    }
    /*
    Method Description: Once clicking on the Select button selects all the flagged on Fares to the next page to Save those.
    */
    public void onSelect(){
                    
        for(ProductWrapper d : productList){
            if(d.selected == true){
                if(theOpp.Type__c == 'Corporate Airfares')
                {                 
                    shoppingCart.add(new Discount_List__c(FareStructure__c = d.product.Id, Proposal__c = theOpp.Id, Qantas_Published_Airfare__c = d.product.Qantas_Published_Airfare__c, Fare_Combination__c = selectedPicklistValue,Segment__c = d.product.Segment__c,Category__c = d.product.Category__c, Network__c = d.product.Network__c, Market__c = d.product.Market__c, Cabin__c = d.product.Cabin__c, Proposal_Type__c = theOpp.Type__c));
                }else if(theOpp.Type__c == 'Qantas Business Savings')
                {
                    Integer discount = 0;
                    Standard_Discounts__c stndrdDiscount;
                    if(selectedPicklistValue != 'International' ) stndrdDiscount = Standard_Discounts__c.getValues(d.product.Name);
                    // CRM - 1994 To Pre-popoulate the Discount values in the Proposal Fare Discount for Mainline - Flex only deal
                    if(selectedPicklistValue == 'Mainline - Flex Only Deal' ) stndrdDiscount = Standard_Discounts__c.getValues(d.product.ProductCode__c);
                    system.debug('Standard discount value other than International' + selectedPicklistValue );
                    // CRM 2054 To Pre-popoulate the Discount values in the Proposal Fare Discount for Mainline - Red-edeal+Q
                    if(selectedPicklistValue == 'Mainline - Red e-Deal + Q' ) stndrdDiscount = Standard_Discounts__c.getValues(d.product.Prod_Code_RededealQ__c);
                    system.debug('Standard discount value other than International' + selectedPicklistValue );
                    if(selectedPicklistValue == 'International') stndrdDiscount = Standard_Discounts__c.getValues(d.product.ProductCode__c);
                    system.debug('Standard discount value of International' + stndrdDiscount );
                    if(stndrdDiscount != null && stndrdDiscount.Fare_Combination__c == selectedPicklistValue)
                    {
                        discount = stndrdDiscount.Default_Discount__c.intValue();
                        system.debug('Discount value is '+ discount );
                    }
                    shoppingCart.add(new Discount_List__c(FareStructure__c = d.product.Id, Proposal__c = theOpp.Id, Qantas_Published_Airfare__c = d.product.Qantas_Published_Airfare__c, Fare_Combination__c = selectedPicklistValue, Segment__c = d.product.Segment__c, Category__c = d.product.Category__c, Network__c = d.product.Network__c, Market__c = d.product.Market__c, Cabin__c = d.product.Cabin__c, Discount__c = discount, QBS_Deafualt_Discount__c = discount, Proposal_Type__c = theOpp.Type__c));
                    system.debug('Inserted values are ' + shoppingCart);
                }
                
            }
        }
        if(shoppingCart.size()==0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select atleast one Booking Class'));
        }else if(shoppingCart.size()>0){
            displaySelected = true;
            displayAvailable = false;
            displayFareCombination = false;
        }
    }
    /*
    Method Description: To paginate all the Fares in a specified count instead of displaying all at a time.
    */
    public void pgntionProducts(){
        limitedProducts = new List<ProductWrapper>();
        
        for(Integer i = OffsetSize; i < (LimitSize + OffsetSize) && i < totalRecs ; i++){
            limitedProducts.add(productList.get(i));        
        }
        
    }
    /*
    Method Description: Displays all the Fare combinations.
    */
    public List<SelectOption> getFareCombinations(){
    
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Standard_Discount_Guidelines__c.Fare_Combination__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
          String str = f.getValue();
          System.debug('**** '+str);
          if(theOpp.Type__c == 'Qantas Business Savings'){ // Added 'Mainline - Flex Only Deal'as per the CRM-1994 & added'Mainline - Red e-Deal + Q' as per the CRM 2054
              if( str == 'Mainline - Red e-Deal' || str == 'Mainline - Red e-Deal + Q' || str == 'International' || str == 'Regional' || str == 'DOM Route Deal' || str == 'Mainline - Flex Only Deal') 
              options.add(new SelectOption(f.getLabel(), f.getValue()));
          }else if(theOpp.Type__c == 'Corporate Airfares'){ // Replaced Mainline - H in B deal with Mainline - Red e-Deal + Q as per the CRM-1990 
                  if( str == 'Mainline - Red e-Deal + Q' || str == 'Mainline - Flex Only Deal' || str == 'Mainline - Red e-Deal' || str == 'Mainline - JCYB Deal'  || str == 'International' || str == 'Regional' || str == 'DOM Route Deal')
              options.add(new SelectOption(f.getLabel(), f.getValue()));
          }
        }       
        return options;
    }
    /*
    Method Description: To get the Fares Ids based on the selected Combination from Fare Discount Guidelines object.
    */
    public List<Id> getBookingClassIds(String pickValue){
        List<Id> allIds = new List<Id>();
        List<Standard_Discount_Guidelines__c> bookingValues = [SELECT Fare_Structure__c FROM Standard_Discount_Guidelines__c where Fare_Combination__c=:pickValue];
        for(Standard_Discount_Guidelines__c sdg : bookingValues){
            allIds.add(sdg.Fare_Structure__c);
        }
        return allIds;
    }
    /*
    Method Description: On change of Fare Combincation changes all the Fares.
    */
    public void fareCombinationChange(){
        
        if(selectedPicklistValue != 'Regional' && selectedPicklistValue != 'DOM Route Deal') disableIndividual = true;
        else disableIndividual = false;
        searchString = '';  
           
        updateAvailableList();
        if(selectedPicklistValue != 'Regional' && selectedPicklistValue != 'DOM Route Deal'){ selectAll = true; onSelectAll(); }
        else { selectAll = false; onSelectAll(); }
                      
    }   
        
    // Pagination Next, Previous buttons
    /*
    Method Description: Goes to next page to display next set of fares in a paginated manner.
    */
    public void next(){
        OffsetSize = LimitSize + OffsetSize;
            
        for(ProductWrapper cCon: limitedProducts) {
            selectedProducts.add(cCon);  
        }   
        pgntionProducts();
    }
    /*
    Method Description: Goes to previous page to display previous set of fares in a paginated manner.
    */
     public void previous(){
         OffsetSize = OffsetSize - LimitSize;
            
         for(ProductWrapper cCon: limitedProducts) {
             selectedProducts.add(cCon);  
         }
         pgntionProducts(); 
      } 
      /*
      Method Description: To enable/ disable previuos button.
      */
      public Boolean getPreviousDisplay(){
             if(OffsetSize == 0)
             return true;
             else
             return false;
        }
        /*
        Method Description: To enable/ disable next button. 
        */
        public Boolean getNextDisplay(){
            if((OffsetSize + LimitSize) > totalRecs)
            return true;
            else  
            return false;
        } 
}