/**********************************************************************************************************************************
 
    Created Date     : 31/08/16 (DD/MM/YYYY)
    Added By         : Vinothkumar Balasubramanian
    Description      : Schedular Class to run the Batch class "Opptycreation_Batch" daily at 7.00 PM AEST (Australian Time)
    JIRA             : CRM - 2053, CRM - 2217 and CRM - 2238
    Version          :V1.0 - 31/08/16 - Initial version
 
**********************************************************************************************************************************/




global class Opptycreation_Batch_Schedular implements Schedulable {

        global void execute(SchedulableContext schedulableContext) {
        Opptycreation_Batch opb = new Opptycreation_Batch();
        Database.executeBatch(opb,1);
        }
}