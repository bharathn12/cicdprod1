@isTest
public class Test_OpptyMaintenanceTriggerHandler{

public static testMethod void OpptymaintenanceUpdate(){
        List<Opportunity_main__c> Opptymaincustom = new List<Opportunity_main__c>();
          List<Opportunity_main__c> OpptymaincustomforQBS = new List<Opportunity_main__c>();
        
        List<Opportunity_Maintenance__c> Opptymain = new List<Opportunity_Maintenance__c>();
        List<Opportunity_Maintenance__c> OpptymainforQBS = new List<Opportunity_Maintenance__c>();


        
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();

        
        Opptymain = TestUtilityDataClassQantas.getOpportunityMaintenanceforCA();
        
                for(Opportunity_Maintenance__c opm : Opptymain){
            
            for(Opportunity_main__c opmc : Opptymaincustom){
            
         opmc.Create_an_Opportunity_before_in_months__c = opm.Create_an_Opportunity_before_in_months__c;
         opmc.Close_Date__c = opm.Close_Date__c;
         opmc.Active__c = opm.Active__c;
         
         System.AssertEquals(opmc.Active__c,true);
         System.AssertEquals(opmc.Close_Date__c,Date.Today());
         System.AssertEquals(opmc.Create_an_Opportunity_before_in_months__c,3);
        }
        }
        
        try{
            Database.Update(Opptymaincustom);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
             for(Opportunity_Maintenance__c Opptymainlist : Opptymain){
          
             Opptymainlist.Create_an_Opportunity_before_in_months__c = 7;
         }
         
                  try{
             Database.Update(Opptymain);
         }catch(Exception e){
             System.debug('Error Occured: '+e.getMessage());
         }
         
                       for(Opportunity_Maintenance__c opmu1 : Opptymain){
            
            for(Opportunity_main__c opmcu1 : Opptymaincustom){
            
         opmcu1.Create_an_Opportunity_before_in_months__c = opmu1.Create_an_Opportunity_before_in_months__c;
        
           System.AssertEquals(opmcu1.Create_an_Opportunity_before_in_months__c,7);
        }
        }
        
        try{
            Database.Update(Opptymaincustom);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
         OpptymainforQBS = TestUtilityDataClassQantas.getOpportunityMaintenanceforQBS();
        
                for(Opportunity_Maintenance__c opmQBS : OpptymainforQBS){
            
            for(Opportunity_main__c opmcQBS : OpptymaincustomforQBS){
            
         opmcQBS.Create_an_Opportunity_before_in_months__c = opmQBS.Create_an_Opportunity_before_in_months__c;
         opmcQBS.Close_Date__c = opmQBS.Close_Date__c;
         opmcQBS.Active__c = opmQBS.Active__c;
         
         System.AssertEquals(opmcQBS.Active__c,true);
         System.AssertEquals(opmcQBS.Close_Date__c,Date.Today());
         System.AssertEquals(opmcQBS.Create_an_Opportunity_before_in_months__c,6);
        }
        }
        
        try{
            Database.Update(OpptymaincustomforQBS);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
                for(Opportunity_Maintenance__c OpptymainlistforQBS : OpptymainforQBS){
             
             OpptymainlistforQBS.Create_an_Opportunity_before_in_months__c = 8;
        
          

             
   
         }
         
                  try{
             Database.Update(OpptymainforQBS);
         }catch(Exception e){
             System.debug('Error Occured: '+e.getMessage());
         }
         
                       for(Opportunity_Maintenance__c opmuQBS : OpptymainforQBS){
            
            for(Opportunity_main__c opmcuQBS : OpptymaincustomforQBS){
            
         opmcuQBS.Create_an_Opportunity_before_in_months__c = opmuQBS.Create_an_Opportunity_before_in_months__c;
         
            System.AssertEquals(opmcuQBS.Create_an_Opportunity_before_in_months__c,8);
    
        }
        }
        
        try{
            Database.Update(OpptymaincustomforQBS);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
  }
  
  public static testMethod void OpptymaintenanceclosedateUpdate(){
        List<Opportunity_main__c> Opptymaincustom = new List<Opportunity_main__c>();
          List<Opportunity_main__c> OpptymaincustomforQBS = new List<Opportunity_main__c>();
        
        List<Opportunity_Maintenance__c> Opptymain = new List<Opportunity_Maintenance__c>();
        List<Opportunity_Maintenance__c> OpptymainforQBS = new List<Opportunity_Maintenance__c>();


        
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();

        
        Opptymain = TestUtilityDataClassQantas.getOpportunityMaintenanceforCA();
        
                for(Opportunity_Maintenance__c opm : Opptymain){
            
            for(Opportunity_main__c opmc : Opptymaincustom){
            
         opmc.Create_an_Opportunity_before_in_months__c = opm.Create_an_Opportunity_before_in_months__c;
         opmc.Close_Date__c = opm.Close_Date__c;
         opmc.Active__c = opm.Active__c;
         
         System.AssertEquals(opmc.Active__c,true);
         System.AssertEquals(opmc.Close_Date__c,Date.Today());
         System.AssertEquals(opmc.Create_an_Opportunity_before_in_months__c,3);
        }
        }
        
        try{
            Database.Update(Opptymaincustom);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
             for(Opportunity_Maintenance__c Opptymainlist : Opptymain){
                 
                 Date myDate = Date.newInstance(2017, 2, 17);
            Opptymainlist.Close_Date__c = mydate.addDays(2);
          
             
         }
         
                  try{
             Database.Update(Opptymain);
         }catch(Exception e){
             System.debug('Error Occured: '+e.getMessage());
         }
         
                       for(Opportunity_Maintenance__c opmu1 : Opptymain){
            
            for(Opportunity_main__c opmcu1 : Opptymaincustom){
            
         opmcu1.Close_Date__c = opmu1.Close_Date__c;
        
           
        }
        }
        
        try{
            Database.Update(Opptymaincustom);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
         OpptymainforQBS = TestUtilityDataClassQantas.getOpportunityMaintenanceforQBS();
        
                for(Opportunity_Maintenance__c opmQBS : OpptymainforQBS){
            
            for(Opportunity_main__c opmcQBS : OpptymaincustomforQBS){
            
         opmcQBS.Create_an_Opportunity_before_in_months__c = opmQBS.Create_an_Opportunity_before_in_months__c;
         opmcQBS.Close_Date__c = opmQBS.Close_Date__c;
         opmcQBS.Active__c = opmQBS.Active__c;
         
         System.AssertEquals(opmcQBS.Active__c,true);
         System.AssertEquals(opmcQBS.Close_Date__c,Date.Today());
         System.AssertEquals(opmcQBS.Create_an_Opportunity_before_in_months__c,6);
        }
        }
        
        try{
            Database.Update(OpptymaincustomforQBS);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
                for(Opportunity_Maintenance__c OpptymainlistforQBS : OpptymainforQBS){
             
                 Date myDate = Date.newInstance(2017, 2, 17);
            OpptymainlistforQBS.Close_Date__c = mydate.addDays(2);
        
          

             
   
         }
         
                  try{
             Database.Update(OpptymainforQBS);
         }catch(Exception e){
             System.debug('Error Occured: '+e.getMessage());
         }
         
                       for(Opportunity_Maintenance__c opmuQBS : OpptymainforQBS){
            
            for(Opportunity_main__c opmcuQBS : OpptymaincustomforQBS){
            
         opmcuQBS.Close_Date__c = opmuQBS.Close_Date__c;
         
            
    
        }
        }
        
        try{
            Database.Update(OpptymaincustomforQBS);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
  }
}