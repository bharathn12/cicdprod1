@isTest
public class Test_proposalProductEntryController{

    public static testMethod void myTest(){
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        List<Proposal__c> propList = new List<Proposal__c>();
        
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();
        
        //Accounts
        accList = TestUtilityDataClassQantas.getAccounts();
        
        // Load Opportunities
         oppList = TestUtilityDataClassQantas.getOpportunities(accList);
         
        // Load Proposals
        propList = TestUtilityDataClassQantas.getProposal(oppList);
        
        // Create Fare Structure
        Fare_Structure__c fs = new Fare_Structure__c(Name = 'J', Active__c = true, Cabin__c = 'Business', 
                                                     Category__c = 'Mainline', Market__c = 'Australia', 
                                                     ProductCode__c = 'DOM-0001', Qantas_Published_Airfare__c = 'JBUS',
                                                     Segment__c = 'Domestic'
                                                     );
        try{
            Database.Insert(fs);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
        // Proposal Main Logic Test
        Test.startTest();
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(propList[0]);
        
        proposalProductEntryController pCon = new proposalProductEntryController(stdController);
        proposalProductEntryController.ProductWrapper pw = new proposalProductEntryController.ProductWrapper(fs);
        pCon.overLimit = false;
        pCon.toUnselect = '';
        pCon.Total = 10;
        pCon.errorMsg = false;
        pCon.toSelect = fs.Id;
        pCon.updateAvailableList();
        pCon.addToShoppingCart();
        pCon.onSelectAll();
        pCon.onSave();
        pCon.onSaveMore();
        pCon.onCancel();        
        pCon.onSelect();
        pCon.pgntionProducts();
        pCon.getFareCombinations();
        pCon.getBookingClassIds('Mainline - H in B deal');
        pCon.fareCombinationChange();
        pCon.next();
        pCon.previous();
        pCon.getPreviousDisplay();
        pCon.getNextDisplay();     
        
        // Update Proposal
        Proposal__c pr = propList[0];
        pr.Type__c = 'Qantas Business Savings';
        
        try{
            Database.Update(pr);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        System.debug('*** Proposal Is: '+pr);        
        
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(pr);
        
        proposalProductEntryController pCon1 = new proposalProductEntryController(stdController1);
        pCon1.selectedPicklistValue = 'All';
        pCon1.onSelect();
        pCon1.onSelectAll();
        pCon1.fareCombinationChange();
        
        Test.stopTest();      
        
    }
}