@isTest
public class Test_ContractBatchTriggerHandler{

   //Added By Praveen For test class failure fix
    @testsetup
    static void createData(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        insert lstConfigData;
    }


    public static testMethod void myTest(){
               Profile pf = [Select Id from Profile where Name = 'System Administrator'];
 User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'User';
        u.Email = 'testuser@test123456789.com';
        u.CompanyName = 'test.com';
        u.Title = 'Test User';
        u.Username = 'testuser@test123456789.com';
        u.Alias = 'testuser';
        u.CommunityNickname = 'Test User';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = pf.Id;
        u.LanguageLocaleKey = 'en_US';
        insert u;
        
          system.runAs(u){ 

            // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();
          
           String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();

          
                     Account acc = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Prospect Account', 
                                      RecordTypeId = prospectRecordTypeId, Migrated__c = false, 
                                      Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false,
                                      Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N' ,Contract_End_Date__c = Date.Today()
                                      );
                                    
                                    
                insert acc;
                
                Contact con = new Contact(FirstName = 'Sample', LastName = acc.Name, AccountId = acc.Id,Job_Role__c='Travel Consultant / Agent',Function__c='Operations',Job_Title__c='Team Leader',Business_Types__c='Agency');
                
                insert con;
                
                
                   Opportunity opp = new Opportunity(  Name = 'Opp'+acc.Name , AccountId = acc.Id,
                                              Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),
                                              StageName = 'Qualify');
                                              
                insert opp;
                
                                   Opportunity opp1 = new Opportunity(  Name = 'Opp1'+acc.Name , AccountId = acc.Id,
                                              Amount = 600000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),
                                              StageName = 'Qualify');
                                              
                insert opp1;
                
                Proposal__c prop = new Proposal__c(Name = 'Proposal '+opp.Name , Account__c = opp.AccountId, Opportunity__c = opp.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false
                                              );
            insert prop;
            
                    Proposal__c prop1 = new Proposal__c(Name = 'Proposal '+opp1.Name , Account__c = opp1.AccountId, Opportunity__c = opp1.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false
                                              );
            insert prop1;
            
                Fare_Structure__c fr = new Fare_Structure__c(Name = 'J', Active__c = true, Cabin__c = 'Business', 
                                                     Category__c = 'Mainline', Market__c = 'Australia', 
                                                     ProductCode__c = 'DOM-0001', Qantas_Published_Airfare__c = 'JBUS',
                                                     Segment__c = 'Domestic'
                                                     );
                                                     
                    insert fr;
                    
                            Standard_Discount_Guidelines__c sd = new Standard_Discount_Guidelines__c(Discount_Threshold_AM__c = 14, Discount_Threshold_NAM__c = 17,
                                                                                 Fare_Combination__c = 'Mainline - H in B deal', Fare_Structure__c = fr.Id,
                                                                                 Maximum_Revenue__c = 999999, Minimum_Revenue__c = 300000
                                                                                 );
                                                                                 
                            insert sd;
                            
                            Discount_List__c disc = new Discount_List__c(FareStructure__c = fr.Id, Proposal__c = prop.Id, 
                                                             Qantas_Published_Airfare__c = fr.Qantas_Published_Airfare__c, 
                                                             Fare_Combination__c = 'Mainline - H in B deal', Segment__c = fr.Segment__c,
                                                             Category__c = fr.Category__c, Network__c = fr.Network__c, 
                                                             Market__c = fr.Market__c, Cabin__c = fr.Cabin__c, 
                                                             Proposal_Type__c = prop.Type__c, Discount__c = 40,
                                                             Approved_by_NAM__c = false, Approved_by_Pricing__c = false,
                                                             DO_Approved__c = false, TeamLead_Approved__c = false
                                                             );
                                                             
                            insert disc;
                            
                            Boolean b=False;
                            
                            Contract__c contr = new Contract__c(Name = 'Contract '+opp.Name , Account__c = prop.Account__c, Opportunity__c = prop.Opportunity__c, Proposal__c = prop.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = b,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signature Required by Customer'                                              
                                              );
                                              b=True;
                                              
                            insert contr;
                            
                     
                            
                                Contract__c contr1 = new Contract__c(Name = 'Contract '+opp1.Name , Account__c = prop1.Account__c, Opportunity__c = prop1.Opportunity__c, Proposal__c = prop1.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = b,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signature Required by Customer'                                              
                                              );
                                              b=True;
                                              
                            insert contr1;
                            
                  
                   
      Account acc1 = [select Contract_End_Date__c from Account where Id = :acc.Id];
            system.assertEquals(acc1.Contract_End_Date__c,contr.Contract_End_Date__c);
            
            
                  Contract__c contr2 = [select Contract_End_Date__c from Contract__c where Id = :contr.Id];
                   // **********Updated the code for BAU Fix Start**************
                   // Date myDate = Date.newInstance(2017, 2, 17);
                      Date myDate = Date.Today(); 
                        contr2.Contract_End_Date__c = mydate.addDays(2);
                   // **********Updated the code for BAU Fix End**************     
                        contr2.Status__c = 'Signed by Customer';

       
            update contr2;
            
            Account acc3 = [select Contract_End_Date__c from Account where Id = :acc.Id];
                 // **********Updated the code for BAU Fix Start**************
                 // Date myDate3 = Date.newInstance(2017, 2, 17);
                    Date myDate3 = Date.Today();    
                        acc3.Contract_End_Date__c = mydate3.addDays(2);
                 // **********Updated the code for BAU Fix End**************     
                        update acc3;
            
            
            
         Account acc2 = [select Contract_End_Date__c from Account where Id = :acc3.Id];
            system.assertEquals(acc2.Contract_End_Date__c,contr2.Contract_End_Date__c);
            

            
            
                          Contract__c contr3 = [select Contract_End_Date__c from Contract__c where Id = :contr1.Id];
                    // **********Updated the code for BAU Fix Start**************
                   // Date myDate1 = Date.newInstance(2017, 2, 17);
                      Date myDate1 = Date.Today();   
                   // **********Updated the code for BAU Fix End**************    
                        contr3.Contract_End_Date__c = mydate1.addDays(4);
                        contr3.Status__c = 'Signed by Customer';

       
            update contr3;
            
            Account acc4 = [select Contract_End_Date__c from Account where Id = :acc.Id];
                 // **********Updated the code for BAU Fix Start**************
                // Date myDate4 = Date.newInstance(2017, 2, 17);
                   Date myDate4 = Date.Today(); 
                // **********Updated the code for BAU Fix End**************          
                        acc4.Contract_End_Date__c = mydate4.addDays(4);
                        update acc4;
            
            
            
              AggregateResult ag1 = [select MAX(Contract_End_Date__c) from Contract__c where Account__c = :acc.Id];
              
               Account acc5 = [select Contract_End_Date__c from Account where Id = :acc.Id];
               
                Test.startTest();
                list<Contract__c> Cons = new list<Contract__c>();
     
                 Set<id> contractId = new Set<id>();
                 for(Contract__c c :Cons )
                 {
                    contractId.add(c.id);
                 }
            ContractBatchTriggerHandler batchObj = new ContractBatchTriggerHandler(contractId);
              List<contract__c> co=[SELECT Status__c, Account__c FROM Contract__c WHERE Status__c = 'Signed by Customer'];
              
              ContractBatchTriggerHandler.contractcommencementdate(co);
              ContractBatchTriggerHandler.contractexpirydate(co);
              ContractBatchTriggerHandler.ContractfieldsupdateonAccount(co);
              
                  
            Database.executeBatch(batchObj,2);
            Test.stopTest();
               
                    system.assertEquals(acc5.Contract_End_Date__c,ag1.get('expr0'));
                    
                }
                }
                }