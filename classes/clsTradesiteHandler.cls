public class clsTradesiteHandler{
   
    public static void processTradesiteAuthorityNumber(List<Case> caseList){
        try{
            Integration_User_Tradesite__c tSite = Integration_User_Tradesite__c.getValues('tradeSiteUser'); 
            Decimal authNum = tSite.NameChangeLastNumber__c;   
            set<String> qicIds = new set<string>();
            map<String,Account> existingQicIds = new map<string,Account>();
            for(Case c: caseList){
                if(c.TradeSite_Request_Id__c != null && !c.Migrated__c){
                    qicIds.add(c.Agency_Code__c);
                }
            }
            map<Id,Account> accMap = new map<Id,Account>([select id,Qantas_Industry_Centre_ID__c from account where Qantas_Industry_Centre_ID__c  != null and type = 'Agency Account' and Qantas_Industry_Centre_ID__c in:qicIds]);
            for(Account a: accMap.values()){
                existingQicIds.put(a.Qantas_Industry_Centre_ID__c,a);
            }
            for(Case c: caseList){
                if(c.TradeSite_Request_Id__c != null && !c.Migrated__c){
                    c.Reason = 'Trade Site';
                    c.Type = 'Name Change Request';
                    c.sub_Type__c = 'Name Change';
                    c.TradeSite_Response_Status__c = tSite.ApprovedText__c;
                    if(!existingQicIds.containsKey(c.Agency_Code__c)){
                        c.Status = 'Closed - Rejected';
                        c.TradeSite_Response_Status__c = tSite.RejectedText__c;
                        c.TradeSite_Response_Message__c = 'Agency Id not found';
                    }else{
                        c.Status = 'Closed - Approved';
                        c.AccountId = existingQicIds.get(c.Agency_Code__c).Id;
                        c.Authority_Number__c = authNum.format().replace(',','') ;
                        authNum = authNum + 1; 
                        if(authNum == tSite.NameChangeEndNumber__c){
                            authNum = tSite.NameChangeStartNumber__c;
                        }                 
                    }
                }
            }
            if(tSite.NameChangeLastNumber__c != authNum){
                tSite.NameChangeLastNumber__c = authNum;
                update tSite;
            } 
        }catch(Exception e){
            system.debug('EXCEPTION_Authority_Number:'+e.getStackTraceString());
        }
    }
    
    public static void processNameChangeRequest(List<Case> caseList){
        try{
            for(Case c: caseList){
                if(c.TradeSite_Request_Id__c != null && !c.Migrated__c){
                    string caseNameChangeReq = '';
                    caseNameChangeReq += '<trad:NameChangeID xmlns:trad="http://www.qantas.com.au/qsoa/schema/TradeSiteIntegration">';
                    caseNameChangeReq += '<ResponseID>'+c.TradeSite_Request_Id__c+'</ResponseID>';
                    caseNameChangeReq += '<Status>'+c.TradeSite_Response_Status__c+'</Status>';// Should always be 'Approved'
                    caseNameChangeReq += '<AuthorityNumber>'+c.Authority_Number__c+'</AuthorityNumber>';
                    caseNameChangeReq += '<Message>'+c.TradeSite_Response_Message__c+'</Message>';
                    caseNameChangeReq += '</trad:NameChangeID>';
                    system.debug(caseNameChangeReq);
                    doTradesiteOutbound(caseNameChangeReq);
                }
            }
        }
        catch(Exception e){
            system.debug('EXCEPTION_NAME_CHANGE_REQUEST:'+e.getStackTraceString());
        }
    }
    
    @Future(callout=true)
    public static void doTradesiteOutbound(string caseNameChangeReq ){
        try{
                String startTime = system.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');//CRM-1341
                String endTime = system.now().addMinutes(10).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');//CRM-1341
                Integration_User_Tradesite__c tSite = Integration_User_Tradesite__c.getValues('tradeSiteUser');
                String tsUserName = tSite.UserName__c; 
                String tsPaswword = tSite.Password__c;
                String tsEndPoint = tSite.endPoint__c;
                String soapBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><wsse:Security soap:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken wsu:Id="unt_pAZ20trJ8X75Bv8I" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsse:Username>';
                soapBody+= tsUserName;
                soapBody+='</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">';
                soapBody+= tsPaswword;
                soapBody+= '</wsse:Password></wsse:UsernameToken><wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><wsu:Created>';
                soapBody+=startTime;
                system.debug('startTime'+startTime);
                soapBody+='</wsu:Created><wsu:Expires>';
                soapBody+=endTime;
                system.debug('endTime'+endTime);

                soapBody+='</wsu:Expires></wsu:Timestamp></wsse:Security></soap:Header><soapenv:Body>';
                soapBody+=caseNameChangeReq;
                soapBody+='</soapenv:Body></soapenv:Envelope>';
                Dom.Document doc = new Dom.Document();
                system.debug(soapBody);
                doc.load(soapBody);
                HttpRequest req = new HttpRequest();

                req.setEndpoint(tsEndPoint);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'text/xml');
                req.setBodyDocument(doc);
                system.debug('@@@'+req);
                
                Http http = new Http();
                if(!test.isRunningTest()){
                    HTTPResponse res = http.send(req);
                    System.debug(res.getBody());
                }        
        }
        catch(Exception e){
            system.debug('EXCEPTION_NAME_CHANGE_REQUEST_WS:'+e.getStackTraceString());
        }
   }
}