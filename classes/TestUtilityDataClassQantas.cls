/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Generic TestData creation Class
Inputs:
Test Class:    Not Required
************************************************************************************************
History
************************************************************************************************
28-Oct-2017    Praveen Sampath   InitialDesign
-----------------------------------------------------------------------------------------------------------------------*/
@isTest
public class TestUtilityDataClassQantas{
    
    public static String prospectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();
    public static String customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
    public static String aquireRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('Aquire Registration').getRecordTypeId();
    public static String amexRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('AEQCC Registration').getRecordTypeId(); 
    public static String qbdRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('QBD Registration').getRecordTypeId();
    public static String businessGovRTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Business and Government').getRecordTypeId();
    
    // Get Account records
    public static List<Account> getAccounts(){
        List<Account> accList = new List<Account>();
        
        for(Integer i=1; i<4; i++){ 
            Account acc = new Account(Name = 'Sample'+i, Active__c = true, Aquire__c = true, Type = 'Prospect Account', 
                                      RecordTypeId = prospectRecordTypeId, Migrated__c = false, 
                                      Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false,
                                      Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N' 
                                      );
            accList.add(acc);
        }   
        
        try{
            Database.Insert(accList);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return accList;
    }
    
    // Get Contacts to the given Accounts
    public static List<Contact> getContacts(List<Account> accList){
        List<Contact> contactList = new List<Contact>();
        
        for(Account acc : accList){
            Contact con = new Contact(FirstName = 'Sample', LastName = acc.Name, AccountId = acc.Id,Function__c = 'IT',Business_Types__c = 'B&G',Job_Role__c ='BDM',Job_Title__c ='Developer',Phone='8907654321',Email='abc@gmail.com');            contactList.add(con);
        }
        
        try{
            Database.Insert(contactList);
        }catch(Exception e){
            System.debug('Error Occure: '+e.getMessage());
        }
        return contactList;
    }
    
    // Get Opportunities to the given Accounts
    public static List<Opportunity> getOpportunities(List<Account> accList){
        List<Opportunity> oppList = new List<Opportunity>();
        
        for(Account acc: accList){
            Opportunity opp = new Opportunity(Name = 'Opp'+acc.Name , AccountId = acc.Id,
                                              Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),
                                              StageName = 'Qualify'
                                              );
            oppList.add(opp);
        }
        
        try{
            Database.Insert(oppList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        return oppList;
    }
    
    // Get Proposal & Discount List to the given Opportunity
    public static List<Proposal__c> getProposal(List<Opportunity> oppList){
        List<Proposal__c> propList = new List<Proposal__c>();
        List<Discount_List__c> discList = new List<Discount_List__c>();
        List<Fare_Structure__c > faresList = new List<Fare_Structure__c >();
        List<Standard_Discount_Guidelines__c> stdgList = new List<Standard_Discount_Guidelines__c>();
        
        for(Opportunity opp: oppList){
            Proposal__c prop = new Proposal__c(Name = 'Proposal '+opp.Name , Account__c = opp.AccountId, Opportunity__c = opp.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false
                                              );
            propList.add(prop);
        }

        try{
            Database.Insert(propList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
        // Fares
        Fare_Structure__c fs = new Fare_Structure__c(Name = 'J', Active__c = true, Cabin__c = 'Business', 
                                                     Category__c = 'Mainline', Market__c = 'Australia', 
                                                     ProductCode__c = 'DOM-0001', Qantas_Published_Airfare__c = 'JBUS',
                                                     Segment__c = 'Domestic'
                                                     );
        faresList.add(fs);                                             
        try{
            Database.Insert(faresList);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
        // Standard Discount Guidelines
        Standard_Discount_Guidelines__c sd = new Standard_Discount_Guidelines__c(Discount_Threshold_AM__c = 14, Discount_Threshold_NAM__c = 17,
                                                                                 Fare_Combination__c = 'Mainline - H in B deal', Fare_Structure__c = fs.Id,
                                                                                 Maximum_Revenue__c = 999999, Minimum_Revenue__c = 300000
                                                                                 );
        try{
            Database.Insert(sd);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }        
        // Discount List
        for(Proposal__c prop: propList){
            for(Fare_Structure__c fr : faresList){
                Discount_List__c disc = new Discount_List__c(FareStructure__c = fr.Id, Proposal__c = prop.Id, 
                                                             Qantas_Published_Airfare__c = fr.Qantas_Published_Airfare__c, 
                                                             Fare_Combination__c = 'Mainline - H in B deal', Segment__c = fr.Segment__c,
                                                             Category__c = fr.Category__c, Network__c = fr.Network__c, 
                                                             Market__c = fr.Market__c, Cabin__c = fr.Cabin__c, 
                                                             Proposal_Type__c = prop.Type__c, Discount__c = 40,
                                                             Approved_by_NAM__c = false, Approved_by_Pricing__c = false,
                                                             DO_Approved__c = false, TeamLead_Approved__c = false
                                                             );
            discList.add(disc);
            }            
        }

        try{
            Database.Insert(discList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }

        return propList;
    }
    
    // Get Contracts by giving proposals as input
    public static List<Contract__c> getContracts(List<Proposal__c> propList){
        List<Contract__c> contrList = new List<Contract__c>();
        Boolean b = false;
        for(Proposal__c opp: propList){
            Contract__c contr = new Contract__c(Name = 'Contract '+opp.Name , Account__c = opp.Account__c, Opportunity__c = opp.Opportunity__c, Proposal__c = opp.Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = b,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signature Required by Customer'                                              
                                              );
            b = true;
            contrList.add(contr);
        }

        try{
            Database.Insert(contrList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        return contrList;
    }
    
    // Get Members by giving Accounts
    public static List<Associated_Person__c> getMembers(List<Account> accList){
        List<Associated_Person__c> memberList = new List<Associated_Person__c>();
        for(Account acc : accList){
            memberList.add(new Associated_Person__c( Account__c = acc.Id, Last_Name__c = 'Member'+acc.Name, First_Name__c = 'Praveen'+acc.Name,
                                                     Frequent_Flyer_Number__c = '7799339937', Active_Profile_Record__c = false,
                                                     Active_QBD_Record__c = false, QBD_Last_Name__c = 'Member'+acc.Name, Salutation__c = 'Mr',
                                                     Sync_Traveller_Override__c = false, Start_Date__c = Date.Today()
                                                     ));
        }
        
        try{
            Database.Insert(memberList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }        
        return memberList;
    }
    
    // Get Cases by giving Accounts
    public static List<Case> getCases(List<Account> accList){
        List<Case> caseList = new List<Case>();
        for(Account acc : accList){
            caseList.add(new Case(AccountId = acc.Id, Type = 'New Traveller',
                                  Sub_Type__c = 'New QBD Traveller added', Status= 'New',
                                  Frequent_Flyer_Number__c = '7799339937'
                                  ));            
        }
        try{
            Database.Insert(caseList);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        } 
        return caseList;
    }
    
     public static List<Opportunity_Maintenance__c> getOpportunityMaintenanceforCA(){
        List<Opportunity_Maintenance__c> OpptyMainList = new List<Opportunity_Maintenance__c>();
        
    
            OpptyMainList.add(new Opportunity_Maintenance__c(Name = 'Sample', Active__c = TRUE, Category__c = 'Corporate Airfares', Create_an_Opportunity_before_in_months__c = 3, 
                                        Close_Date__c = Date.Today()));
                                        
                                     
           
         
            
                                        
                                     
            
        
        try{
            Database.Insert(OpptyMainList);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return OpptyMainList;
    }
    
      public static List<Opportunity_Maintenance__c> getOpportunityMaintenanceforQBS(){
        List<Opportunity_Maintenance__c> OpptyMainList1 = new List<Opportunity_Maintenance__c>();
        
          OpptyMainList1.add(new Opportunity_Maintenance__c(Name = 'Sample2', Active__c = TRUE, Category__c = 'Qantas Business Savings', Create_an_Opportunity_before_in_months__c = 6, 
                                        Close_Date__c = Date.Today()));
                                        
                                              try{
            Database.Insert(OpptyMainList1);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return OpptyMainList1;
    }  
    // ACR CRM 2306
    public static List<AccountContactRelation> getAccountContactRelation(){
        List<AccountContactRelation> AccConRel = new List<AccountContactRelation>();
        AccountContactRelation newAcc = new AccountContactRelation(Contactid ='0039000001otQDw',Accountid ='0019000001Q7PxM',Function__c = 'IT',Business_Type__c = 'B&G',Job_Role__c ='BDM',Job_Title__c ='Developer',Related_Phone__c='8907654321',Related_Email__c='abc@gmail.com');   
        try {
            Database.insert(newAcc);
         } catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return AccConRel;
       }
       
       public static List<Contact> getContact(){
        List<Contact> allCont = new List<Contact>();
        allCont.add(new Contact(lastname= 'Giri',firstname='Priya',Accountid ='0019000001Q7PxM',Function__c = 'IT',Business_Types__c = 'B&G',Job_Role__c ='BDM',Job_Title__c ='Developer',Phone='8907654321',Email='abc@gmail.com'));
        try {
            Database.insert(allCont);
         } catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return allCont;
       }
     
    
    
    // Get the 2 User based on the Given Profile, Department, Role
    public static List<User> getUsers(String profileName, String dName, String roleName){
        List<User> allUsers = new List<User>();
        Map<String, Profile> profMap = getProfileMap();
        Map<String, UserRole> roleMap = getRoles();
        Id role;
        if(roleMap.get(roleName) != null) 
            role = roleMap.get(roleName).Id; 
        else 
            role = null; 

        System.debug('111111111'+role);
        System.debug('222222222'+roleName);
        for(Integer i=1; i<3; i++){
            User u = new User(LastName = 'TestUser'+i, Alias = 'testu'+i , 
                              Email = 'testuser'+i+'@sample.com', Username = 'testusername'+i+'@sample.com', 
                              CommunityNickname = 'testu'+i, Department = dName, ProfileId = profMap.get(profileName).Id, 
                              UserRoleId = role, EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                              TimeZoneSidKey = 'Australia/Sydney'
                              );
            
            allUsers.add(u);
        }
        
        try{
            Database.Insert(allUsers);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return allUsers;
    }   
    
    // Get the Map of all profiles, (Profile Name, Profile)
    public static Map<String, Profile> getProfileMap(){
        Map<String, Profile> profileMap = new Map<String, Profile>();
        List<Profile> allProfiles = [SELECT Name, Id FROM Profile];
        
        for(Profile p: allProfiles){
            profileMap.put(p.Name, p);
        }
        return profileMap;
    }
    
    // Get the Map of all Roles, (Role Name, Role)
    public static Map<String, UserRole> getRoles(){
        Map<String, UserRole> rolesMap = new Map<String, UserRole>();
        List<UserRole> allRoles = [SELECT Name, Id FROM UserRole];
        
        for(UserRole ur : allRoles){
            rolesMap.put(ur.Name, ur);
        }
        return rolesMap;
    } 
    
    // Get Trigger Status Custom setting
    public static List<Trigger_Status__c> enableTriggers(){
        List<Trigger_Status__c> tsList = new List<Trigger_Status__c>();
        
        Trigger_Status__c ts = new Trigger_Status__c();
        ts.Name = 'AccountTeam';
        ts.Active__c = true;
        
        tsList.add(ts);
        
        Trigger_Status__c ts1 = new Trigger_Status__c();
        ts1.Name = 'AccountOwnerTrigger';
        ts1.Active__c = true;
        
        tsList.add(ts1);
        
        Trigger_Status__c ts2 = new Trigger_Status__c();
        ts2.Name = 'ContractTrigger';
        ts2.Active__c = true;
        
        tsList.add(ts2);
        
        Trigger_Status__c ts3 = new Trigger_Status__c();
        ts3.Name = 'CreateOpportunities';
        ts3.Active__c = true;
        
        tsList.add(ts3);
        
        Trigger_Status__c ts4 = new Trigger_Status__c();
        ts4.Name = 'DiscountListTrigger';
        ts4.Active__c = true;
        
        tsList.add(ts4);
        
        Trigger_Status__c ts5 = new Trigger_Status__c();
        ts5.Name = 'OpportunityTeam';
        ts5.Active__c = true;
        
        tsList.add(ts5);
        
        Trigger_Status__c ts6 = new Trigger_Status__c();
        ts6.Name = 'ProposalTrigger';
        ts6.Active__c = true;
        
        tsList.add(ts6);
        
        Trigger_Status__c ts7 = new Trigger_Status__c();
        ts7.Name = 'MemberTrigger';
        ts7.Active__c = true;
        
        
        tsList.add(new Trigger_Status__c(Name = 'trgTradeSiteResponses', Active__c = true));
        tsList.add(ts7);
        
        /*Gourav Bhardwaj */
        Trigger_Status__c ts8 = new Trigger_Status__c();
        ts8.Name = 'AccountEligibilityLogTrigger';
        ts8.Active__c = true;
        tsList.add(ts8);
        
        Trigger_Status__c ts9 = new Trigger_Status__c();
        ts9.Name = 'ContractAccountEligibilityLogTrigger';
        ts9.Active__c = true;
        tsList.add(ts9);

        Trigger_Status__c ts10 = new Trigger_Status__c();
        ts10.Name = 'OpportunityMaintenanceTrigger';
        ts10.Active__c = true;
        tsList.add(ts10);
        
        Trigger_Status__c ts11 = new Trigger_Status__c();
        ts11.Name = 'AccconRshipUpdate';
        ts11.Active__c = true;
        tsList.add(ts11);
        
        Trigger_Status__c ts12 = new Trigger_Status__c();
        ts12.Name = 'AccountAirlineLeveltrigger';
        ts12.Active__c = true;
        tsList.add(ts12);

        Trigger_Status__c AccountDeleteTrigger = new Trigger_Status__c();
        AccountDeleteTrigger.Name = 'AccountDeleteTrigger';
        AccountDeleteTrigger.Active__c = false;
        tsList.add(AccountDeleteTrigger);
                
        try{
            Database.Insert(tsList);
        }Catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return tsList;
    }
    
    // Get Account Revenue Custom setting
    public static List<Account_Revenue__c> getAccountRevenueCSetting(){
        List<Account_Revenue__c> customSetting = new List<Account_Revenue__c>();
        
        Account_Revenue__c ar1 = new Account_Revenue__c();
        ar1.Name = 'Marketing Acquisition & Sales Team';
        ar1.Maximum__c = 199999;
        ar1.Minimum__c = 0;
        customSetting.add(ar1);
        
        Account_Revenue__c ar2 = new Account_Revenue__c();
        ar2.Name = 'New Business Development Team';
        ar2.Minimum__c = 1000000;
        ar2.Maximum__c = 2147483647;   
        customSetting.add(ar2);
        
        Account_Revenue__c ar3 = new Account_Revenue__c();
        ar3.Name = 'MAS Team';
        ar3.Minimum__c = 200000;
        ar3.Maximum__c = 299999;   
        customSetting.add(ar3);    
                
        try{
            Database.Insert(customSetting);    
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return customSetting;
    } 
    
      // AEQCC Product registration extensions
    public static List<AEQCC_Variance__c> getrelatedCustObjRecs(){
        List<AEQCC_Variance__c> conList = New List<AEQCC_Variance__c>();
        List<Product_Registration__c> Prodreglist = new List<Product_Registration__c>();
        
        Product_Registration__c Pr = new Product_Registration__c(name = 'Test Product Registration',Account__c='0019000001Q7EID');
        AEQCC_Variance__c Aeq = new AEQCC_Variance__c (Reporting_Period__c = 'Jan-Oct',Product_Link__c='a2cO00000013Djs',CurrencyIsoCode='AU');
     try{
            Database.Insert(Pr);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
     try{
            Database.Insert(Aeq);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
       } 
        for(AEQCC_Variance__c aev:pr.AEQCC_Variance__r){
               conList.add(aev); 
        }        
        prodregextensions controllerTest = new prodregextensions(new ApexPages.StandardController(pr));     
        controllerTest.getrelatedCustObjRecs();
        return conList;
    }
    
        // AEQCC Rebate extensions
    public static List<AEQCC_Rebates__c> getrelatedCustObjRecs1(){
        List<AEQCC_Rebates__c> Reblist = New List<AEQCC_Rebates__c>();
        List<Product_Registration__c> Prodreglist = new List<Product_Registration__c>();
        
        Product_Registration__c Pr = new Product_Registration__c(name = 'Test AEQCC REBATE',Account__c='0019000001Q7EID');
        AEQCC_Rebates__c Aeqreb = new AEQCC_Rebates__c (Rebate_Year__c = '2015',Rebate_Tier__c ='2 %',Rebate_term__c='Jul-Dec',Rebate_Payment__c=753,Cheque_Amount__c=826.61,Rebate_Sent_to__c='Adam Simon Po Box 103 Port Melbourne Vic 3207',Prod_Reg_Link__c='a0G9000001BfHSj');
     try{
            Database.Insert(Pr);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
     try{
            Database.Insert(Aeqreb);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
       } 
        for(AEQCC_Rebates__c aereb:pr.AEQCC_Rebates__r ){
               Reblist.add(aereb); 
        }        
        prodregextensions controllerTest = new prodregextensions(new ApexPages.StandardController(pr));     
        controllerTest.getrelatedCustObjRecs();
        return Reblist;
    }
    
    // Crate Case type Custom settings
    public static List<QBD_Case_Type__c > getCaseTypeCSetting(){
        List<QBD_Case_Type__c > customSetting = new List<QBD_Case_Type__c >();
        
        customSetting.add(new QBD_Case_Type__c (Name = 'New QBD Registration', PickList_value__c = 'New QBD Registration'));
        customSetting.add(new QBD_Case_Type__c (Name = 'New Traveller', PickList_value__c = 'New Traveller'));
        
        try{
            Database.Insert(customSetting);    
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return customSetting;
    }  
    
    // Crate Case subtype Custom settings
    public static List<QBD_Case_Subtype__c> getCaseSubtypeCSetting(){
        List<QBD_Case_Subtype__c> customSetting = new List<QBD_Case_Subtype__c>();
        
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Agency Account Identified', PickList_value__c = 'Agency Account Identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Duplicate Members identified', PickList_value__c = 'Duplicate Members identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Duplicate QBD Registration Identified', PickList_value__c = 'Duplicate QBD Registration Identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Duplicate QBD Traveller identified', PickList_value__c = 'Duplicate QBD Traveller identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Last Name Mismatch', PickList_value__c = 'Last Name Mismatch'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Member updated with QBD Traveller', PickList_value__c = 'Existing Member updated with QBD Traveller details'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple accounts - Aquire membership', PickList_value__c = 'Multiple matching accounts with Aquire membership'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Accounts Identified', PickList_value__c = 'Multiple Accounts Identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Agency Accounts Identified', PickList_value__c = 'Multiple Agency Accounts Identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple QBD Accounts identified', PickList_value__c = 'Multiple QBD Accounts identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'New QBD Traveller added', PickList_value__c = 'New QBD Traveller added'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'No Account identified', PickList_value__c = 'No Account identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'No matching accounts Aquire membership', PickList_value__c = 'No matching accounts with Aquire membership'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'No QBD Account identified', PickList_value__c = 'No QBD Account identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'QBD Registration add New Customer', PickList_value__c = 'QBD Registration added for New Customer'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'QBD Registration update Customer', PickList_value__c = 'QBD Registration updated for Existing Customer'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Unique Account Identified', PickList_value__c = 'Unique Account Identified'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'New Member', PickList_value__c = 'New Member'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Existing Travel Arranger', PickList_value__c = 'Existing Travel Arranger'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Unique Member identified by email', PickList_value__c = 'Unique Member identified by email'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Unique Member identified by name', PickList_value__c = 'Unique Member identified by name'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Unidentified Error', PickList_value__c = 'Unidentified Error'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Duplicate Members identified by email', PickList_value__c = 'Duplicate Members identified by email'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Travel Arrangers - manual', PickList_value__c = 'Multiple Travel Arrangers - manual'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Travel Arrangers - auto', PickList_value__c = 'Multiple Travel Arrangers - auto'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Travellers identified by name', PickList_value__c = 'Multiple Travellers identified by name')); 
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Travellers - manual', PickList_value__c = 'Multiple Travellers - manual'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Multiple Travellers - auto', PickList_value__c = 'Multiple Travellers - auto'));
        customSetting.add(new QBD_Case_Subtype__c(Name = 'Unique Traveller identified by name', PickList_value__c = 'Unique Traveller identified by name'));   

        Database.Insert(customSetting);   
         
        return customSetting;
    }   
    
    // Crate Case subtype Custom settings
    public static List<Regex_Validator__c> getRegexValidatorCSetting(){
        List<Regex_Validator__c> customSetting = new List<Regex_Validator__c>();
        
        customSetting.add(new Regex_Validator__c(Name = 'Email', Regex__c = '(^([^\\s<>\\\\]+)@([^\\s<>\\\\]+))(.)*$'));
        
        try{
            Database.Insert(customSetting);    
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        return customSetting;
    }

   /*public static CreateQBRRegistration.AccountInput craeteAccountInput(){
        CreateQBRRegistration.AccountInput accInput = new CreateQBRRegistration.AccountInput();
        accInput.abn = '99999999999';
        accInput.membershipNumber = '123';
        accInput.entityName = 'MyAccTest';
        accInput.entityType = 'Test Entity Type';
        accInput.numberOfEmployees = '50';
        accInput.rewardTier = '';
        accInput.gstRegistered = 'Y';
        return accInput;
    } 

   public static CreateQBRRegistration.ProductRegistrationInput createProductRegInput(){
        CreateQBRRegistration.ProductRegistrationInput prdRedInput = new CreateQBRRegistration.ProductRegistrationInput();
        prdRedInput.membershipNumber = '123';
        prdRedInput.accountStatus = 'Active';
        prdRedInput.membershipExpiryDate = System.today();
        prdRedInput.entityName = 'MyCompanyTest';
        prdRedInput.entityType = 'Test Entity Type';
        prdRedInput.programBusinessName = 'Program Business Name';
        prdRedInput.accountAddressType = 'Billing';
        prdRedInput.accountAddressLine1 = '123 Fake Street';
        prdRedInput.accountAddressLine2 = 'Fake Line 2';
        prdRedInput.accountSuburbTownCity = 'Sydney';
        prdRedInput.accountStateCounty = 'NSW';
        prdRedInput.accountPostalCode = '2000';
        prdRedInput.accountCountry = 'Australia';
        prdRedInput.industry = 'Banking';
        prdRedInput.subIndustry = 'Finance';
        prdRedInput.companyTurnover = '2000';
        prdRedInput.totalActiveCards = '100';
        prdRedInput.frequencyOfDomesticTravel = '101-200';
        prdRedInput.frequencyOfInternationalTravel = '201-300';
        prdRedInput.businessDomesticAnnualSpend = '10000';
        prdRedInput.businessInternationalAccountSpend = '50000';
        prdRedInput.numberOfFrequentlyTravellingEmployees = '40';
        prdRedInput.numberofOccasionallyTravellingEmployees = '20';
        prdRedInput.numberOfRarelyTravellingEmployees = '10';
        prdRedInput.hasQantasClubCorporateMembership = '5';
        prdRedInput.qantasClubCorporateMembership = '5';
        prdRedInput.enrolmentDate = System.today().addDays(-30);
        prdRedInput.pointBalance = '1000';
        return prdRedInput;
    } 
    

    public static CreateQBRRegistration.ContactInput createContactInput(){
        CreateQBRRegistration.ContactInput conInput = new CreateQBRRegistration.ContactInput();
        conInput.membershipNumber = '123';
        conInput.accountNomineeCustomerId ='1111';
        conInput.accountHolderFlag = true;
        conInput.accountNomineeUserFlag = true;
        conInput.keyTravelDecisionMakerFlag = true;
        conInput.keyTravelCoordinatorFlag = true;
        conInput.accountNomineeStatus = 'Active';
        conInput.accountNomineeStatusReason = 'Status Reason';
        conInput.accountNomineeSalutation = 'Mr.';
        conInput.accountNomineeFirstName = 'Praveen';
        conInput.accountNomineeLastName = 'Sampath';
        conInput.accountNomineeEmailAddress = 'prav.samp@test.com';
        conInput.accountNomineeContactNumber = '+61 0000001';
        conInput.accountNomineeQFFNumber = '1111';
        conInput.accountNomineeCompanyRole = 'Senior Con';
        conInput.accountNomineeMarketingPreference1 = true;
        conInput.accountNomineeMarketingPreference2 = true;
        conInput.accountNomineeMarketingPreference3 = true;
        conInput.accountNomineeMarketingPreference4 = true;
        conInput.accountNomineeAirTravellerStatus = true;
        return conInput;
    }*/
    

    public static Account createAccount(){
        Account acc = new Account(Name = 'Sample Acc', Active__c = true, Aquire__c = true, Type = 'Prospect Account', 
                                      RecordTypeId = prospectRecordTypeId, Migrated__c = false, 
                                      Estimated_Total_Air_Travel_Spend__c = 0, Manual_Revenue_Update__c = false,
                                      Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N' 
                                      );
        return acc;
    }

    public static QantasConfigData__c createQantasConfigData( String name, String value ){
        QantasConfigData__c objConfigData = new QantasConfigData__c();
        objConfigData.Name = name;
        objConfigData.Config_Value__c = value;
        return objConfigData;
    }

    public static void insertQantasConfigData(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Prd Reg Aquire Registration Rec Type', 'Aquire Registration'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Prd Reg QBD Registration Rec Type', 'QBD Registration'));
        insert lstConfigData;    
    }
    
    /*public static CreateQBRRegistration.responseToIFLY createExpectedResponse( String membershipNumber){
        Product_Registration__c objPrdReg = [SELECT Membership_Number__c, Account__r.Dealing_flag__c, Account__r.Contract_Commencement_Date__c, 
                                             Account__r.Contract_Expiry_Date__c, Account__r.Aquire_Override__c, Account__r.Agency__c, 
                                             Account__r.AMEX_response_Code__c, MCA_Number__c, Account__r.Aquire_Eligibility_f__c, Account__r.SME_Deal__c, 
                                             Account__r.QBD__c, Account__r.Aquire_Membership_Number__c FROM Product_registration__c 
                                             WHERE Membership_Number__c = :membershipNumber LIMIT 1];
       
        CreateQBRRegistration.responseToIFLY response = new CreateQBRRegistration.responseToIFLY();
        response.sourceCode = 'QUEST';
        response.batchDate = String.valueOf(Date.today());
        response.recordType = '2';
        response.batchSequenceNumber = 000000001;
        response.membershipNumber = objPrdReg.Account__r.Aquire_Membership_Number__c;
        response.corporateDealWithQantas = objPrdReg.Account__r.Dealing_flag__c;
        response.dealingCustomerStartDate = objPrdReg.Account__r.Contract_Commencement_Date__c;
        response.dealingCustomerEndDate = objPrdReg.Account__r.Contract_Expiry_Date__c;
        response.smeOverride = objPrdReg.Account__r.Aquire_Override__c;
        response.amexCoBrandedCardPartner = 'N';
        response.travelAgentOfQantas = objPrdReg.Account__r.Agency__c;
        response.amexEligibility = objPrdReg.Account__r.AMEX_response_Code__c;
        response.mcaNumber = objPrdReg.MCA_Number__c;
        response.aquireEligibility = objPrdReg.Account__r.Aquire_Eligibility_f__c;
        response.smeDeal = objPrdReg.Account__r.SME_Deal__c;
        response.qbdMember = objPrdReg.Account__r.QBD__c;
        return response;
    } */

    public static List<QBD_Online_Office_ID__c> createQBDOnlineOfficeId(){
        List<QBD_Online_Office_ID__c> lstQBDOff = new List<QBD_Online_Office_ID__c>();
        for(Integer i = 1; i<=4; i++){
            QBD_Online_Office_ID__c qbdOffice = new QBD_Online_Office_ID__c();
            qbdOffice.Name = String.valueOf(i);
            qbdOffice.GDS_QBR_Discount_Code__c = 'GBD000'+String.valueOf(i);
            qbdOffice.Key_Contact_Office_ID__c = 'KeyOff000'+String.valueOf(i);
            qbdOffice.Qantas_Club_Discount_Code__c = 'QCD000'+String.valueOf(i);
            lstQBDOff.add(qbdOffice);
        }
        return lstQBDOff;
    }

    public static Product_Registration__c createProductRegistration(Id accId, String type){
        Product_Registration__c objPrdReg = new Product_Registration__c();
        objPrdReg.Stage_Status__c = 'Active';
        objPrdReg.Account__c = accId;
        if(type == 'Acquire')
            objPrdReg.RecordTypeId = aquireRecordTypeId;
        else if(type == 'QBD Registration')
            objPrdReg.RecordTypeId = qbdRecordTypeId;
        objPrdReg.Active__c = true;
        return objPrdReg;
    }

    public static Account_Logs_Tracker__c createAccountLogsTracker(String objName, String fieldLabel, String value, String name){
        Account_Logs_Tracker__c accLog = new Account_Logs_Tracker__c();
        accLog.Field_Label__c = fieldLabel;
        accLog.Active__c = true;
        accLog.Object__c = objName;
        accLog.Value__c = value;
        accLog.Name = name;
        return accLog;
    }

    public static List<Trigger_Status__c>createAccountTriggerSetting(){
      List<Trigger_Status__c> lsttrgStatus = new List<Trigger_Status__c>();
      Trigger_Status__c trgStatus = new Trigger_Status__c();
      trgStatus.Name = 'AccountAirlineLeveltrigger';
      trgStatus.Active__c = false;
      lsttrgStatus.add(trgStatus);

      Trigger_Status__c AccountTeam = new Trigger_Status__c();
      AccountTeam.Name = 'AccountTeam';
      AccountTeam.Active__c = false;
      lsttrgStatus.add(AccountTeam);
      
      Trigger_Status__c AccountEligibilityLogTrigger = new Trigger_Status__c();
      AccountEligibilityLogTrigger.Name = 'AccountEligibilityLogTrigger';
      AccountEligibilityLogTrigger.Active__c = false;
      lsttrgStatus.add(AccountEligibilityLogTrigger);
      
      Trigger_Status__c AccountDeleteTrigger = new Trigger_Status__c();
      AccountDeleteTrigger.Name = 'AccountDeleteTrigger';
      AccountDeleteTrigger.Active__c = false;
      lsttrgStatus.add(AccountDeleteTrigger);
      return lsttrgStatus;
    }
}