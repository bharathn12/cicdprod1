/**********************************************************************************************************************************
 
    Created Date     :  24/04/17 (DD/MM/YYYY)
    Added By         : Karpagam Swaminathan
    Description      : This class contains unit tests for validating the behavior of CustTransitionQBStoQBR_Batch_Schedular class
    JIRA             : CRM-2522
    Version          :V1.0 - 24/04/17 - Initial version
 
**********************************************************************************************************************************/
@isTest
private class Test_CustTransQBStoQBR_Batch_Schedular {

   // CRON expression: midnight on March 15.
   // Because this is a test, job executes
   // immediately after Test.stopTest().
   public static String CRON_EXP = '0 0 0 15 3 ? 2099';

   static testmethod void testScheduler() {
      Test.startTest();

          // Schedule the test job
          String jobId = System.schedule('ScheduleApexClassTest',
                            CRON_EXP, 
                            new CustTransitionQBStoQBR_Batch_Schedular());
             
          // Get the information from the CronTrigger API object
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
             NextFireTime
             FROM CronTrigger WHERE id = :jobId];
    
          // Verify the expressions are the same
          System.assertEquals(CRON_EXP, 
             ct.CronExpression);
    
          // Verify the job has not run
          System.assertEquals(0, ct.TimesTriggered);
    
          // Verify the next time the job will run
          System.assertEquals('2099-03-15 00:00:00', 
             String.valueOf(ct.NextFireTime));
      
      Test.stopTest();    

   }
}